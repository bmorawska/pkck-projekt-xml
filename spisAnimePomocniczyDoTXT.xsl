<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns="http://www.w3.org/1999/xhtml">

	<xsl:output method="text" version="1.0" encoding="UTF-8"/>

<xsl:template match="/dokument">
====================================================================================================================================================================================================================================
-------------------------------------------------------------------------------------------------------------SPIS ANIME-------------------------------------------------------------------------------------------------------------
====================================================================================================================================================================================================================================

------------------------------------------------------------
---------------------------ŹRÓDŁA---------------------------
------------------------------------------------------------
<xsl:apply-templates select="spis_anime/źródło"/>

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------LISTA ANIME-------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
NAZWA                                   ŹRÓDŁO                 STUDIO                  DATA WYDANIA    TYP         GATUNKI                                             LICZBA ODC.   DŁUGOŚĆ ODC.   OCENA SPOŁECZNOŚCI   NASZA OCENA
<xsl:apply-templates select="spis_anime/źródło/pozycje_anime/anime">
	<xsl:sort select="nazwa" data-type="text" order="ascending"/>
</xsl:apply-templates>

------------------------------------------------------------
-------------------------STATYSTYKI-------------------------
------------------------------------------------------------
<xsl:apply-templates select="statystyki"/>

------------------------------------------------------------
---------------------------TWÓRCY---------------------------
------------------------------------------------------------
<xsl:apply-templates select="autorzy"/>
</xsl:template>

	<xsl:template match="źródło">
		<xsl:text>&#8226;</xsl:text>
		<xsl:value-of select="nazwa_źródła"/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>

		
	<xsl:template match="anime">
		<xsl:value-of select="concat(nazwa, substring('                                       ', string-length(nazwa)),
                                ../../nazwa_źródła, substring('                      ', string-length(../../nazwa_źródła)),
                                studio, substring('                       ', string-length(studio)),
                                data_wydania, substring('               ', string-length(data_wydania)),
                                typ, substring('           ', string-length(typ)),
								gatunki, substring('                                                   ', string-length(gatunki)),
								liczba_odcinków, substring('             ', string-length(liczba_odcinków)),
								długość_odcinka, substring('              ', string-length(długość_odcinka)),
								ocena_społeczności, substring('                    ', string-length(ocena_społeczności)),
								nasza_ocena
                            )"/>
	<xsl:text>&#10;</xsl:text>
	</xsl:template>
	
	<xsl:template match="statystyki">
		<xsl:value-of select="concat('Liczba źródeł:                           ', liczba_źródeł)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba studiów:                          ', liczba_studiów)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba gatunków:                         ', liczba_gatunków)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime:                            ', liczba_anime)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (akcja):                    ', liczba_anime_akcja)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (dramat):                   ', liczba_anime_dramat)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (fantasy):                  ', liczba_anime_fantasy)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (historia):                 ', liczba_anime_historia)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (horror):                   ', liczba_anime_horror)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (komedia):                  ', liczba_anime_komedia)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (muzyka):                   ', liczba_anime_muzyka)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (okruchy życia):            ', liczba_anime_okruchy_życia)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (przygoda):                 ', liczba_anime_przygoda)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (psychologia):              ', liczba_anime_psychologia)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (romans):                   ', liczba_anime_romans)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (science fiction):          ', liczba_anime_science_fiction)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (sport):                    ', liczba_anime_sport)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (tajemnica):                ', liczba_anime_tajemnica)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Liczba anime (zjawiska nadprzyrodzone):  ', liczba_anime_zjawiska_nadprzyrodzone)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Suma odcinków:                           ', suma_odcinków)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Łączna liczba minut:                     ', łączna_liczba_minut)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Średnia ocen społecznosci:               ', średnia_ocen_społeczności)"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:value-of select="concat('Średnia naszych ocen:                    ', średnia_naszych_ocen)"/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>

		
	<xsl:template match="autorzy">
		<xsl:apply-templates select="autor"/>
	</xsl:template>
	
	<xsl:template match="autor">
		<xsl:value-of select="concat('          ', imię, ' ', nazwisko, ' - ', numer_indeksu)"/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>
</xsl:stylesheet>

