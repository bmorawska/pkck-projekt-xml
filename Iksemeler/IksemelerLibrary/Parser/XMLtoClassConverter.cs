﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IksemelerLibrary.Parser
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.animeNamespace.com")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.animeNamespace.com", IsNullable = false)]
    public partial class dokument
    {

        private string linkField;

        private string tytułField;

        private ObservableCollection<autorzyAutor> autorzyField;

        private anime_dane anime_daneField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string link
        {
            get
            {
                return this.linkField;
            }
            set
            {
                this.linkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public string tytuł
        {
            get
            {
                return this.tytułField;
            }
            set
            {
                this.tytułField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Namespace = "")]
        [System.Xml.Serialization.XmlArrayItemAttribute("autor", IsNullable = false)]
        public ObservableCollection<autorzyAutor> autorzy
        {
            get
            {
                return this.autorzyField;
            }
            set
            {
                this.autorzyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public anime_dane anime_dane
        {
            get
            {
                return this.anime_daneField;
            }
            set
            {
                this.anime_daneField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class link
    {

        private string hrefField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string href
        {
            get
            {
                return this.hrefField;
            }
            set
            {
                this.hrefField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class autorzyAutor
    {

        private string imięField;

        private string nazwiskoField;

        private uint indeksField;

        private byte semestrField;

        private string specjalizacjaField;

        /// <remarks/>
        public string imię
        {
            get
            {
                return this.imięField;
            }
            set
            {
                this.imięField = value;
            }
        }

        /// <remarks/>
        public string nazwisko
        {
            get
            {
                return this.nazwiskoField;
            }
            set
            {
                this.nazwiskoField = value;
            }
        }

        /// <remarks/>
        public uint indeks
        {
            get
            {
                return this.indeksField;
            }
            set
            {
                this.indeksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte semestr
        {
            get
            {
                return this.semestrField;
            }
            set
            {
                this.semestrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string specjalizacja
        {
            get
            {
                return this.specjalizacjaField;
            }
            set
            {
                this.specjalizacjaField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class anime_dane
    {

        private ObservableCollection<anime_daneAnimation> spis_animeField;

        private ObservableCollection<anime_daneTyp> typyField;

        private ObservableCollection<anime_daneGatunek> gatunkiField;

        private ObservableCollection<anime_daneStudio> studiaField;

        private ObservableCollection<anime_daneŹródło> źródłaField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("animation", IsNullable = false)]
        public ObservableCollection<anime_daneAnimation> spis_anime
        {
            get
            {
                return this.spis_animeField;
            }
            set
            {
                this.spis_animeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("typ", IsNullable = false)]
        public ObservableCollection<anime_daneTyp> typy
        {
            get
            {
                return this.typyField;
            }
            set
            {
                this.typyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("gatunek", IsNullable = false)]
        public ObservableCollection<anime_daneGatunek> gatunki
        {
            get
            {
                return this.gatunkiField;
            }
            set
            {
                this.gatunkiField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("studio", IsNullable = false)]
        public ObservableCollection<anime_daneStudio> studia
        {
            get
            {
                return this.studiaField;
            }
            set
            {
                this.studiaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("źródło", IsNullable = false)]
        public ObservableCollection<anime_daneŹródło> źródła
        {
            get
            {
                return this.źródłaField;
            }
            set
            {
                this.źródłaField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneAnimation
    {

        private anime_daneAnimationInformacje informacjeField;

        private anime_daneAnimationOcena ocenaField;

        private string studioIDREFField;

        /// <remarks/>
        public anime_daneAnimationInformacje informacje
        {
            get
            {
                return this.informacjeField;
            }
            set
            {
                this.informacjeField = value;
            }
        }

        /// <remarks/>
        public anime_daneAnimationOcena ocena
        {
            get
            {
                return this.ocenaField;
            }
            set
            {
                this.ocenaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string studioIDREF
        {
            get
            {
                return this.studioIDREFField;
            }
            set
            {
                this.studioIDREFField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneAnimationInformacje
    {

        private string nazwaField;

        private string nazwa_oryginalnaField;

        private anime_daneAnimationInformacjeTyp_ref typ_refField;

        private anime_daneAnimationInformacjeŹródło_ref źródło_refField;

        private anime_daneAnimationInformacjeData_wydania data_wydaniaField;

        private ObservableCollection<anime_daneAnimationInformacjeGatunek_ref> lista_gatunkówField;

        private byte liczba_odcinkówField;

        private anime_daneAnimationInformacjeDługość długośćField;

        /// <remarks/>
        public string nazwa
        {
            get
            {
                return this.nazwaField;
            }
            set
            {
                this.nazwaField = value;
            }
        }

        /// <remarks/>
        public string nazwa_oryginalna
        {
            get
            {
                return this.nazwa_oryginalnaField;
            }
            set
            {
                this.nazwa_oryginalnaField = value;
            }
        }

        /// <remarks/>
        public anime_daneAnimationInformacjeTyp_ref typ_ref
        {
            get
            {
                return this.typ_refField;
            }
            set
            {
                this.typ_refField = value;
            }
        }

        /// <remarks/>
        public anime_daneAnimationInformacjeŹródło_ref źródło_ref
        {
            get
            {
                return this.źródło_refField;
            }
            set
            {
                this.źródło_refField = value;
            }
        }

        /// <remarks/>
        public anime_daneAnimationInformacjeData_wydania data_wydania
        {
            get
            {
                return this.data_wydaniaField;
            }
            set
            {
                this.data_wydaniaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("gatunek_ref", IsNullable = false)]
        public ObservableCollection<anime_daneAnimationInformacjeGatunek_ref> lista_gatunków
        {
            get
            {
                return this.lista_gatunkówField;
            }
            set
            {
                this.lista_gatunkówField = value;
            }
        }

        public string lista_gatunków_w_stringu
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (var gatunek in lista_gatunkówField)
                {
                    sb.Append(gatunek.Value);
                    if (!gatunek.Equals(lista_gatunków.Last()))
                    {
                        sb.Append(", ");
                    }
                }

                return sb.ToString();
            }
        }

        /// <remarks/>
        public byte liczba_odcinków
        {
            get
            {
                return this.liczba_odcinkówField;
            }
            set
            {
                this.liczba_odcinkówField = value;
            }
        }

        /// <remarks/>
        public anime_daneAnimationInformacjeDługość długość
        {
            get
            {
                return this.długośćField;
            }
            set
            {
                this.długośćField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneAnimationInformacjeTyp_ref
    {

        private string typIDREFField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string typIDREF
        {
            get
            {
                return this.typIDREFField;
            }
            set
            {
                this.typIDREFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneAnimationInformacjeŹródło_ref
    {

        private string źródłoIDREFField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string źródłoIDREF
        {
            get
            {
                return this.źródłoIDREFField;
            }
            set
            {
                this.źródłoIDREFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneAnimationInformacjeData_wydania
    {

        private string dzieńField;

        private string miesiącField;

        private string rokField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string dzień
        {
            get
            {
                return this.dzieńField;
            }
            set
            {
                this.dzieńField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string miesiąc
        {
            get
            {
                return this.miesiącField;
            }
            set
            {
                this.miesiącField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string rok
        {
            get
            {
                return this.rokField;
            }
            set
            {
                this.rokField = value;
            }
        }

        public DateTime data_wydania
        {
            get
            {
                CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
                string dataString = miesiącField  + "/" + dzieńField + "/" + rokField;
                DateTime data = DateTime.Parse(dataString, culture);
                return data;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneAnimationInformacjeGatunek_ref
    {

        private string gatunekIDREFField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string gatunekIDREF
        {
            get
            {
                return this.gatunekIDREFField;
            }
            set
            {
                this.gatunekIDREFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneAnimationInformacjeDługość
    {

        private string jednostkaField;

        private byte valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string jednostka
        {
            get
            {
                return this.jednostkaField;
            }
            set
            {
                this.jednostkaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public byte Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneAnimationOcena
    {

        private decimal średnia_ocenaField;

        private decimal moja_ocenaField;

        /// <remarks/>
        public decimal średnia_ocena
        {
            get
            {
                return this.średnia_ocenaField;
            }
            set
            {
                this.średnia_ocenaField = value;
            }
        }

        /// <remarks/>
        public decimal moja_ocena
        {
            get
            {
                return this.moja_ocenaField;
            }
            set
            {
                this.moja_ocenaField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneTyp
    {

        private string typIDField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string typID
        {
            get
            {
                return this.typIDField;
            }
            set
            {
                this.typIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneGatunek
    {

        private string gatunekIDField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string gatunekID
        {
            get
            {
                return this.gatunekIDField;
            }
            set
            {
                this.gatunekIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneStudio
    {

        private string nazwa_studiaField;

        private ushort rok_założeniaField;

        private byte liczba_produkcjiField;

        private string studioIDField;

        /// <remarks/>
        public string nazwa_studia
        {
            get
            {
                return this.nazwa_studiaField;
            }
            set
            {
                this.nazwa_studiaField = value;
            }
        }

        /// <remarks/>
        public ushort rok_założenia
        {
            get
            {
                return this.rok_założeniaField;
            }
            set
            {
                this.rok_założeniaField = value;
            }
        }

        /// <remarks/>
        public byte liczba_produkcji
        {
            get
            {
                return this.liczba_produkcjiField;
            }
            set
            {
                this.liczba_produkcjiField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string studioID
        {
            get
            {
                return this.studioIDField;
            }
            set
            {
                this.studioIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class anime_daneŹródło
    {

        private string źródłoIDField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string źródłoID
        {
            get
            {
                return this.źródłoIDField;
            }
            set
            {
                this.źródłoIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class autorzy
    {

        private ObservableCollection<autorzyAutor> autorField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("autor")]
        public ObservableCollection<autorzyAutor> autor
        {
            get
            {
                return this.autorField;
            }
            set
            {
                this.autorField = value;
            }
        }
    }
}
