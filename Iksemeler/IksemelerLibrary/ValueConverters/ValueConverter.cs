﻿using IksemelerLibrary.Parser;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IksemelerLibrary.ValueConverters
{
    public class ValueConverter
    {
        public String GetTypIDREFfromValue(String typ)
        {
            switch (typ)
            {
                case "Seria TV": return "TV";
                case "Film": return "Movie";
                case "OVA": return "OVA";
                default: return "NoType";
            }
        }

        public String GetŹródłoIDREFfromValue(String źródło)
        {
            switch (źródło)
            {
                case "Produkcja oryginalna": return "Original";
                case "Manga": return "Manga";                  
                case "Light Novel": return "LN";               
                case "Visual Novel": return "VN";              
                case "Gra": return "Game";                     
                case "Powieść": return "Novel";                 
                default: return "NoŹródło";
            } 
        }

        public String GetGatunekIDREFfromValue(String typ)
        {
            switch (typ)
            {
               case "Akcja": return "Action";
               case "Dramat": return "Drama";
               case "Fantasy": return "Fantasy";
               case "Historia": return "Historical";
               case "Horror": return "Horror";
               case "Komedia": return "Comedy";
               case "Muzyka": return "Music";
               case "Okruchy życia": return "SliceOfLife";
               case "Przygoda": return "Adventure";
               case "Psychologia": return "Psychological";
               case "Romans": return "Romance";
               case "Scifi": return "Scifi";
               case "Sport": return "Sports";
               case "Tajemnica": return "Mystery";
               case "Zjawiska nadprzyrodzone": return "Supernatural";
               default: return "noGatunek";
            }
        }

        public String GetDzieńFromDate(DateTime date)
        {
            string day = FormatDate(date.Day.ToString());
            return day;
        }

        public String GetMiesiącFromDate(DateTime date)
        {
            string month = FormatDate(date.Month.ToString());
            return month;
        }

        public String GetRokFromDate(DateTime date)
        {
            string year = date.Year.ToString();
            return year;
        }

        public ObservableCollection<anime_daneAnimationInformacjeGatunek_ref>  GetCollectionOfGatunki(List<String> gatunki)
        {
            ObservableCollection<anime_daneAnimationInformacjeGatunek_ref> kolekcja =
                new ObservableCollection<anime_daneAnimationInformacjeGatunek_ref>();

            foreach (var gatunek in gatunki)
            {
                anime_daneAnimationInformacjeGatunek_ref g = new anime_daneAnimationInformacjeGatunek_ref();
                g.Value = gatunek.ToString();
                g.gatunekIDREF = GetGatunekIDREFfromValue(g.Value);
                kolekcja.Add(g);
            }

            return kolekcja;
        }

        public String GetJednostka()
        {
            return "min";
        }

        private String FormatDate(String date)
        {
            String newDate = date;
            if (date.Length < 2)
            {
                newDate = "0" + newDate;
            }

            return newDate;
        }

    }
}
