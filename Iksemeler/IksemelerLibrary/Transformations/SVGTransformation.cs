﻿using System;
using System.Xml.Xsl;

namespace IksemelerLibrary.Transformations
{
    public class SVGTransformation : ITransformation
    {
        public bool Transform(String svg, String xsl, String xml)
        {
            try
            {
                XslCompiledTransform xslt = new XslCompiledTransform();
                xslt.Load(xsl);

                xslt.Transform(xml, svg);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
