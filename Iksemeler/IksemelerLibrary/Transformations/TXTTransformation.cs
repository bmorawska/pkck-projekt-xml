﻿using System;
using System.Xml.Xsl;

namespace IksemelerLibrary.Transformations
{
    public class TXTTransformation : ITransformation
    {
        public bool Transform(String txt, String xsl, String xml)
        {
            try
            {
                XslCompiledTransform xslt = new XslCompiledTransform();
                xslt.Load(xsl);

                xslt.Transform(xml, txt);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
