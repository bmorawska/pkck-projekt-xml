﻿using System;
using System.Xml.Xsl;

namespace IksemelerLibrary.Transformations
{
    public class XHTMLTransformation : ITransformation
    {
        public bool Transform(String xhtml, String xsl, String xml)
        {
            try
            {
                XslCompiledTransform xslt = new XslCompiledTransform();
                xslt.Load(xsl);

                xslt.Transform(xml, xhtml);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
