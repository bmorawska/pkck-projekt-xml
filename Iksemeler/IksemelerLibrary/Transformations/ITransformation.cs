﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IksemelerLibrary.Transformations
{
    public interface ITransformation
    {
        //Zwraca true jeśli się udało, false jeśli nie.
        bool Transform(String output, String xsl, String xml);
    }
}
