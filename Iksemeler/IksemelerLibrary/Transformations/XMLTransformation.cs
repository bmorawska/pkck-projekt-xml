﻿using System;
using System.Xml.Xsl;

namespace IksemelerLibrary.Transformations
{
    public class XMLTransformation : ITransformation
    {
        public bool Transform(String xmlout, String xsl, String xmlin)
        {
            try
            {
                XslCompiledTransform xslt = new XslCompiledTransform();
                xslt.Load(xsl);

                xslt.Transform(xmlin, xmlout);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
