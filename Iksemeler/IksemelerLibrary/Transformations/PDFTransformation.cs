﻿using Fonet;
using System;
using System.IO;
using System.Xml.Xsl;

namespace IksemelerLibrary.Transformations
{
    public class PDFTransformation : ITransformation
    {
        public bool Transform(String pdf, String xsl, String xml)
        {
            string temporaryFilePath = xml;
            int index = temporaryFilePath.LastIndexOf("/");
            if (index > 0)
            {
                temporaryFilePath = temporaryFilePath.Substring(0, index);
            }
            temporaryFilePath = temporaryFilePath + "tmp.fo";

            try
            {
                // To transform to pdf fonet driver is needed.
                // https://archive.codeplex.com/?p=fonet
                // TODO Not working :-(

                XslCompiledTransform xslt = new XslCompiledTransform();
                xslt.Load(xsl);

                xslt.Transform(xml, "intermediate.fo");
                FonetDriver driver = FonetDriver.Make();
                driver.Render("intermediate.fo", pdf);
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
               // File.Delete(temporaryFilePath);
            }
        }
    }
}
