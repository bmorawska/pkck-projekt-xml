<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:svg="http://www.w3.org/2000/svg" 
    xmlns="http://www.w3.org/2000/svg" version="1.0" 
    xmlns:xlink="http://www.w3.org/1999/xlink">

    <xsl:output method="xml" media-type="image/svg" encoding="utf-8" doctype-public="-//W3C//DTD SVG 1.1//EN" doctype-system="http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd"/>

    <xsl:template match="/dokument">
        <svg width="1000" height="800">

            <xsl:call-template name="style"/>

            <!-- Tło -->
            <rect width="1000" height="800" style="fill:#2d3e50;" />

            <xsl:call-template name="tytuł"/>
            <xsl:call-template name="autorzy"/>
            <xsl:call-template name="statystyki-wykres-słupkowy"/>
            <xsl:call-template name="statystyki-paski"/>
            <xsl:call-template name="statystyki-koła"/>

        </svg>
    </xsl:template>

    <xsl:template name="style">
        <defs>
            <style>
                @import url("https://fonts.googleapis.com/css?family=Alfa+Slab+One|Lato:400,400i,700&amp;subset=latin-ext");
            </style>
        </defs>
    </xsl:template>

    <xsl:template name="tytuł">
        <svg>
            <text x="700" y="50" font-family="Alfa Slab One" font-size="32" style="fill: #1d262f ">STATYSTYKI</text>
            <text x="685" y="100" font-family="Alfa Slab One" font-size="60" style="fill: #97a4b1">ANIME</text>
            <text x="100" y="30" font-family="Alfa Slab One" font-size="14" transform="translate(125,50) rotate(90, 125, 75) translate(-75, -635)" style="fill: #51667b">
                NASZYCH
            </text>
        </svg>
    </xsl:template>

    <xsl:template name="autorzy">
        <text x="980" y="350" font-family="Alfa Slab One" font-size="18" transform="rotate(270, 980, 350)" style="fill: #1d262f ">Barbara Morawska 210279</text>
        <text x="980" y="650" font-family="Alfa Slab One" font-size="18" transform="rotate(270, 980, 650)" style="fill: #1d262f ">Andrzej Sasinowski 210312</text>
    </xsl:template>

    <xsl:template name="statystyki-wykres-słupkowy">
        <svg>
            <!--Deklaracja zmiennych -->
            <xsl:variable name="liczba_anime_akcja" select="/dokument/statystyki/liczba_anime_akcja * 10"/>
            <xsl:variable name="liczba_anime_dramat" select="/dokument/statystyki/liczba_anime_dramat * 10"/>
            <xsl:variable name="liczba_anime_fantasy" select="/dokument/statystyki/liczba_anime_fantasy * 10"/>
            <xsl:variable name="liczba_anime_historia" select="/dokument/statystyki/liczba_anime_historia * 10"/>
            <xsl:variable name="liczba_anime_horror" select="/dokument/statystyki/liczba_anime_horror * 10"/>
            <xsl:variable name="liczba_anime_komedia" select="/dokument/statystyki/liczba_anime_komedia * 10"/>
            <xsl:variable name="liczba_anime_muzyka" select="/dokument/statystyki/liczba_anime_muzyka * 10"/>
            <xsl:variable name="liczba_anime_okruchy_życia" select="/dokument/statystyki/liczba_anime_okruchy_życia * 10"/>
            <xsl:variable name="liczba_anime_przygoda" select="/dokument/statystyki/liczba_anime_przygoda * 10"/>
            <xsl:variable name="liczba_anime_psychologia" select="/dokument/statystyki/liczba_anime_psychologia * 10"/>
            <xsl:variable name="liczba_anime_romans" select="/dokument/statystyki/liczba_anime_romans * 10"/>
            <xsl:variable name="liczba_anime_sport" select="/dokument/statystyki/liczba_anime_sport * 10"/>
            <xsl:variable name="liczba_anime_tajemnica" select="/dokument/statystyki/liczba_anime_tajemnica * 10"/>
            <xsl:variable name="liczba_zjawiska_nadprzyrodzone" select="/dokument/statystyki/liczba_zjawiska_nadprzyrodzone * 10"/>

            <!--Rysowanie układu współrzędnych -->
            <xsl:call-template name="układ_współrzędnych"/>

            <!--Rysowanie słupków -->
            <svg>
                <rect id="rect_liczba_anime_akcja" x="90" y="{748 - $liczba_anime_akcja}" width="40" height="{$liczba_anime_akcja}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_dramat" x="150" y="{748 - $liczba_anime_dramat}" width="40" height="{$liczba_anime_dramat}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_fantasy" x="210" y="{748 - $liczba_anime_fantasy}" width="40" height="{$liczba_anime_fantasy}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_historia" x="270" y="{748 - $liczba_anime_historia}" width="40" height="{$liczba_anime_historia}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_horror" x="330" y="{748 - $liczba_anime_horror}" width="40" height="{$liczba_anime_horror}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_komedia" x="390" y="{748 - $liczba_anime_komedia}" width="40" height="{$liczba_anime_komedia}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_muzyka" x="450" y="{748 - $liczba_anime_muzyka}" width="40" height="{$liczba_anime_muzyka}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_okruchy_życia" x="510" y="{748 - $liczba_anime_okruchy_życia}" width="40" height="{$liczba_anime_okruchy_życia}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_przygoda" x="570" y="{748 - $liczba_anime_przygoda}" width="40" height="{$liczba_anime_przygoda}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_psychologia" x="630" y="{748 - $liczba_anime_psychologia}" width="40" height="{$liczba_anime_psychologia}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_romans" x="690" y="{748 - $liczba_anime_romans}" width="40" height="{$liczba_anime_romans}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_sport" x="750" y="{748 - $liczba_anime_sport}" width="40" height="{$liczba_anime_sport}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>
            <svg>
                <rect id="rect_liczba_anime_tajemnica" x="810" y="{748 - $liczba_anime_tajemnica}" width="40" height="{$liczba_anime_tajemnica}" style="fill:#decc00" onmouseover="this.style.fill = '#ff0000';" onmouseout="this.style.fill = '#decc00';" />
            </svg>>

            <!--Podpisy słupków -->
            <svg>
                <text x="90" y="{748}" transform="rotate(270, 90, 748) translate({$liczba_anime_akcja + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">akcja</text>
            </svg>
            <svg>
                <text x="150" y="{748}" transform="rotate(270, 150, 748) translate({$liczba_anime_dramat + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">dramat</text>
            </svg>
            <svg>
                <text x="210" y="{748}" transform="rotate(270, 210, 748) translate({$liczba_anime_fantasy + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">fantasy</text>
            </svg>
            <svg>
                <text x="270" y="{748}" transform="rotate(270, 270, 748) translate({$liczba_anime_historia + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">historia</text>
            </svg>
            <svg>
                <text x="330" y="{748}" transform="rotate(270, 330, 748) translate({$liczba_anime_horror + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">horror</text>
            </svg>
            <svg>
                <text x="390" y="{748}" transform="rotate(270, 390, 748) translate({$liczba_anime_komedia + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">komedia</text>
            </svg>
            <svg>
                <text x="450" y="{748}" transform="rotate(270, 450, 748) translate({$liczba_anime_muzyka + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">muzyka</text>
            </svg>
            <svg>
                <text x="510" y="{748}" transform="rotate(270, 510, 748) translate({$liczba_anime_okruchy_życia + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">okruchy życia</text>
            </svg>
            <svg>
                <text x="570" y="{748}" transform="rotate(270, 570, 748) translate({$liczba_anime_przygoda + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">przygodowe</text>
            </svg>
            <svg>
                <text x="630" y="{748}" transform="rotate(270, 630, 748) translate({$liczba_anime_psychologia + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">psychologia</text>
            </svg>
            <svg>
                <text x="690" y="{748}" transform="rotate(270, 690, 748) translate({$liczba_anime_romans + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">romans</text>
            </svg>
            <svg>
                <text x="750" y="{748}" transform="rotate(270, 750, 748) translate({$liczba_anime_sport + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">sport</text>
            </svg>
            <svg>
                <text x="810" y="{748}" transform="rotate(270, 810, 748) translate({$liczba_anime_tajemnica + 20 }, 25)" font-family="Alfa Slab One" font-size="15" style="fill: #51667b ">tajemnica</text>
            </svg>

        </svg>
    </xsl:template>


    <xsl:template name="układ_współrzędnych">
        <!--Oś X -->
        <line x1="50" y1="750" x2="900" y2="750" style="stroke:#51667b;stroke-width:2" />
        <polygon points="900,740 900,760 920,750" style="fill:#51667b" />
        <!--Oś Y -->
        <line x1="70" y1="770" x2="70" y2="500" style="stroke:#51667b;stroke-width:2" />
        <polygon points="60,500 80, 500 70,480" style="fill:#51667b" />

        <!-- podziałka -->
        <line x1="65" y1="710" x2="75" y2="710" style="stroke:#51667b;stroke-width:2" />
        <line x1="65" y1="670" x2="75" y2="670" style="stroke:#51667b;stroke-width:2" />
        <line x1="65" y1="630" x2="75" y2="630" style="stroke:#51667b;stroke-width:2" />
        <line x1="65" y1="590" x2="75" y2="590" style="stroke:#51667b;stroke-width:2" />
        <line x1="65" y1="550" x2="75" y2="550" style="stroke:#51667b;stroke-width:2" />
        <text x="45" y="715" font-family="Lato" font-size="15" style="fill: #51667b ">4</text>
        <text x="45" y="675" font-family="Lato" font-size="15" style="fill: #51667b ">8</text>
        <text x="45" y="635" font-family="Lato" font-size="15" style="fill: #51667b ">12</text>
        <text x="45" y="595" font-family="Lato" font-size="15" style="fill: #51667b ">16</text>
        <text x="45" y="555" font-family="Lato" font-size="15" style="fill: #51667b ">20</text>
        <svg>
            <text x="20" y="600" transform="rotate(270, 20, 600) translate(-60,10)" font-family="Lato" font-size="15" style="fill: #51667b ">liczba produkcji</text>
        </svg>

    </xsl:template>

    <xsl:template name="statystyki-paski">

        <xsl:variable name="łączna_liczba_minut" select="/dokument/statystyki/łączna_liczba_minut"/>
        <xsl:variable name="suma_odcinków" select="/dokument/statystyki/suma_odcinków "/>
        <svg>
            <rect x="90" y="300" width="300" height="50" style="fill:#decc00" />
            <polygon points="390,300 390,350 560,380 560,280" style="fill:#c3b408" />
            <polygon points="560,380 560,280  890,280 890,380" style="fill:#decc00" />
        </svg>
        <svg>
            <rect x="90" y="350" width="300" height="50" style="fill:#ffa70f" />
            <polygon points="390,400 390,350 560,380 560,470" style="fill:#d08e1b" />
            <polygon points="560,380 560,470 890,470 890,380" style="fill:#ffa70f" />
        </svg>

        <text x="120" y="335" font-family="Alfa Slab One" font-size="30" style="fill: #2d3e50 ">
            <xsl:value-of select="$łączna_liczba_minut"/>
 minut</text>
        <text x="580" y="325" font-family="Alfa Slab One" font-size="22" style="fill: #2d3e50 ">Tyle czasu spędziliśmy </text>
        <text x="580" y="350" font-family="Alfa Slab One" font-size="22" style="fill: #2d3e50 ">na oglądaniu anime </text>

        <text x="120" y="385" font-family="Alfa Slab One" font-size="30" style="fill: #2d3e50 ">
            <xsl:value-of select="$suma_odcinków"/>
 odcinków</text>
        <text x="580" y="415" font-family="Alfa Slab One" font-size="22" style="fill: #2d3e50 ">Tyle odcinków łącznie </text>
        <text x="580" y="445" font-family="Alfa Slab One" font-size="22" style="fill: #2d3e50 ">obejrzeliśmy </text>

    </xsl:template>

    <xsl:template name="statystyki-koła">

        <xsl:variable name="liczba_źródeł" select="/dokument/statystyki/liczba_źródeł"/>
        <xsl:variable name="liczba_studiów" select="/dokument/statystyki/liczba_studiów "/>
        <xsl:variable name="liczba_gatunków" select="/dokument/statystyki/liczba_gatunków"/>
        <xsl:variable name="średnia_ocen_społeczności" select="/dokument/statystyki/średnia_ocen_społeczności"/>
        <xsl:variable name="średnia_naszych_ocen" select="/dokument/statystyki/średnia_naszych_ocen"/>

        <script type="text/javascript">
            <![CDATA[
            function changeColor(evt) {
                var target = evt.target;
                var color = target.getAttribute("fill");
                color = "#decc00"
                target.setAttribute("fill",color);
            }

            function revertColor(evt) {
                var target = evt.target;
                var color = target.getAttribute("fill");
                color = "#51667b"
                target.setAttribute("fill",color);

                var infotext = document.getElementById('infotext');
                var rectangle = document.getElementById('inforect');
                rectangle.setAttribute("visibility", "hidden"); 
                infotext.setAttribute("visibility", "hidden"); 
            }

            function showDetails(comment) {
                var xMouse = event.clientX;
                var yMouse = event.clientY;

                var rectangle = document.getElementById('inforect');

                rectangle.setAttribute("visibility", "visible"); 
                rectangle.setAttribute("x", xMouse);
                rectangle.setAttribute("y", yMouse);

                var rectLength = comment.length * 10 + 20;
                rectangle.setAttribute("width", rectLength);

                var infotext = document.getElementById('infotext');
                infotext.setAttribute("x", xMouse + 20);
                infotext.setAttribute("y", yMouse + 30);
                infotext.setAttribute("visibility", "visible"); 
                infotext.textContent = comment;

            }
            ]]>
        </script>

        <circle cx="120" cy="100" r="60" fill="#51667b" onmouseenter="changeColor(evt)" onmouseleave="revertColor(evt)" onclick="showDetails('średnia ocen społeczności')">
            <animate attributeName="r" values="45;60;45" begin="0s" dur="6s" fill="freeze" repeatCount="indefinite" />
        </circle>
        <circle cx="230" cy="160" r="50" fill="#51667b" onmouseenter="changeColor(evt)" onmouseleave="revertColor(evt)" onclick="showDetails('liczba źródeł')">
            <animate attributeName="r" values="62;40;62" begin="0s" dur="4s" fill="freeze" repeatCount="indefinite" />
        </circle>
        <circle cx="360" cy="100" r="70" fill="#51667b" onmouseenter="changeColor(evt)" onmouseleave="revertColor(evt)" onclick="showDetails('średnia naszych ocen')">
            <animate attributeName="r" values="55;80;55" begin="0s" dur="7s" fill="freeze" repeatCount="indefinite" />
        </circle>
        <circle cx="490" cy="140" r="40" fill="#51667b" onmouseenter="changeColor(evt)" onmouseleave="revertColor(evt)" onclick="showDetails('liczba gatunków')">
            <animate attributeName="r" values="45;30;45" begin="0s" dur="5s" fill="freeze" repeatCount="indefinite" />
        </circle>
        <circle cx="600" cy="100" r="50" fill="#51667b" onmouseenter="changeColor(evt)" onmouseleave="revertColor(evt)" onclick="showDetails('liczba studiów')">
            <animate attributeName="r" values="40;50;40" begin="0s" dur="6s" fill="freeze" repeatCount="indefinite" />
        </circle>

        <text x="85" y="110" font-family="Alfa Slab One" font-size="32" style="fill: #2d3e50 ">
            <xsl:value-of select="$średnia_ocen_społeczności"/>
        </text>
        <text x="220" y="170" font-family="Alfa Slab One" font-size="30" style="fill: #2d3e50 ">
            <xsl:value-of select="$liczba_źródeł"/>
        </text>
        <text x="325" y="115" font-family="Alfa Slab One" font-size="36" style="fill: #2d3e50 ">
            <xsl:value-of select="$średnia_naszych_ocen"/>
        </text>
        <text x="470" y="150" font-family="Alfa Slab One" font-size="30" style="fill: #2d3e50 ">
            <xsl:value-of select="$liczba_gatunków"/>
        </text>
        <text x="580" y="110" font-family="Alfa Slab One" font-size="30" style="fill: #2d3e50 ">
            <xsl:value-of select="$liczba_studiów"/>
        </text>

        <rect id="inforect" x="500" y="150" width="150" height="50" style="fill:#decc00" visibility="hidden" />
        <text id="infotext" x="520" y="180" font-family="Alfa Slab One" font-size="14" style="fill: #2d3e50 " visibility="hidden"> Ups...</text>

    </xsl:template>

</xsl:stylesheet>