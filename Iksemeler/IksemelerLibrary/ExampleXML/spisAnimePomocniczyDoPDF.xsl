<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
	
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>	

<xsl:template match="/dokument">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	
		<fo:layout-master-set>
			<fo:simple-page-master master-name="anime-page">
				<fo:region-body margin-left="1.5cm" margin-right="1.5cm" margin-top="0.5cm"/>
				<fo:region-before background-color="rgb(230,230,250)" extent="100%"/> <!-- for the color of the page -->
			</fo:simple-page-master>
		</fo:layout-master-set>
		
		<fo:page-sequence master-reference="anime-page">
			<fo:flow flow-name="xsl-region-body">
				<fo:block font-family="Helvetica" font-size="22pt" font-weight="bold" text-align="center" color="rgb(50,0,0)" margin-bottom="20px">
					<xsl:value-of select="tytuł"/>
				</fo:block>
				<fo:block font-family="Times New Roman" font-size="17pt" font-style="italic" text-align="center" color="rgb(100,100,100)" padding="10px"> Źródła </fo:block>
				<fo:block-container keep-together="always" font-family="Verdana" font-size="14pt" font-style="italic" text-align="center">
					<xsl:apply-templates select="spis_anime/źródło"/>
				</fo:block-container>
				<fo:block-container keep-together="always" padding-bottom="40px">
					<fo:block font-family="Times New Roman" font-size="17pt" font-style="italic" text-align="center" color="rgb(100,100,100)" padding="10px"> Produkcje oryginalne </fo:block>
					<xsl:apply-templates select="spis_anime/źródło[@źródłoID='Original']/pozycje_anime"/>
				</fo:block-container>
				<fo:block-container keep-together="always" padding-bottom="50px">
					<fo:block font-family="Times New Roman" font-size="17pt" font-style="italic" text-align="center" color="rgb(100,100,100)" padding="10px"> Na podstawie mangi </fo:block>
					<xsl:apply-templates select="spis_anime/źródło[@źródłoID='Manga']/pozycje_anime"/>
				</fo:block-container>
				<fo:block-container keep-together="always" padding-bottom="50px">
					<fo:block font-family="Times New Roman" font-size="17pt" font-style="italic" text-align="center" color="rgb(100,100,100)" padding="10px"> Na podstawie Light Novel </fo:block>
					<xsl:apply-templates select="spis_anime/źródło[@źródłoID='LN']/pozycje_anime"/>
				</fo:block-container>
				<fo:block-container keep-together="always" padding-bottom="50px">
					<fo:block font-family="Times New Roman" font-size="17pt" font-style="italic" text-align="center" color="rgb(100,100,100)" padding="10px"> Na podstawie Visual Novel </fo:block>
					<xsl:apply-templates select="spis_anime/źródło[@źródłoID='VN']/pozycje_anime"/>
				</fo:block-container>
				<fo:block-container keep-together="always" padding-bottom="50px">
					<fo:block font-family="Times New Roman" font-size="17pt" font-style="italic" text-align="center" color="rgb(100,100,100)" padding="10px"> Na podstawie gry </fo:block>
					<xsl:apply-templates select="spis_anime/źródło[@źródłoID='Game']/pozycje_anime"/>
				</fo:block-container>
				<fo:block-container keep-together="always" padding-bottom="50px">
					<fo:block font-family="Times New Roman" font-size="17pt" font-style="italic" text-align="center" color="rgb(100,100,100)" padding="10px"> Na podstawie powieści </fo:block>
					<xsl:apply-templates select="spis_anime/źródło[@źródłoID='Novel']/pozycje_anime"/>
				</fo:block-container>
				<fo:block-container keep-together="always" margin-top="30px" padding-bottom="50px">
					<fo:block font-family="Times New Roman" font-size="17pt" font-style="italic" text-align="center" color="rgb(100,100,100)" padding="10px"> Statystyki </fo:block>
					<xsl:apply-templates select="statystyki"/>
				</fo:block-container>
				<fo:block-container keep-together="always" margin-top="260px" padding-bottom="15px">
					<fo:block font-family="Times New Roman" font-size="22pt" font-style="italic" text-align="center" color="rgb(100,100,100)" margin-top="10px" padding="10px"> Autorzy </fo:block>
					<xsl:apply-templates select="autorzy"/>
				</fo:block-container>
			</fo:flow>
		</fo:page-sequence>
	</fo:root>
</xsl:template>

<xsl:template match="źródło">
			<fo:block>
				<fo:inline font-family="Courier" font-size="16pt">»</fo:inline>
				<fo:inline> <xsl:value-of select="nazwa_źródła"/> </fo:inline>
				<fo:inline font-family="Courier" font-size="16pt">«</fo:inline>
            </fo:block>
</xsl:template>

<xsl:template match="pozycje_anime">
	<fo:table border="0.5pt solid black" font-family="Times New Roman" text-align="center" keep-together="always">
		<fo:table-body width="100%">
			<fo:table-row background-color="rgb(100,100,100)">
				<fo:table-cell padding="5px" border="0.5pt solid black" display-align="center">
					<fo:block text-align="center" font-weight="bold" font-size="8pt" color="white"> NAZWA </fo:block>
                </fo:table-cell>
				<fo:table-cell padding="5px" border="0.5pt solid black" display-align="center">
					<fo:block text-align="center" font-weight="bold" font-size="8pt" color="white"> STUDIO </fo:block>
                </fo:table-cell>
				<fo:table-cell padding="5px" border="0.5pt solid black" display-align="center">
					<fo:block text-align="center" font-weight="bold" font-size="8pt" color="white"> DATA WYDANIA </fo:block>
                </fo:table-cell>
				<fo:table-cell width="50px" padding="5px" border="0.5pt solid black" display-align="center">
					<fo:block text-align="center" font-weight="bold" font-size="8pt" color="white"> TYP </fo:block>
                </fo:table-cell>
				<fo:table-cell padding="5px" border="0.5pt solid black" display-align="center">
					<fo:block text-align="center" font-weight="bold" font-size="8pt" color="white"> GATUNKI </fo:block>
                </fo:table-cell>
				<fo:table-cell padding="5px" border="0.5pt solid black" display-align="center">
					<fo:block text-align="center" font-weight="bold" font-size="8pt" color="white"> LICZBA ODC. </fo:block>
                </fo:table-cell>
				<fo:table-cell padding="5px" border="0.5pt solid black" display-align="center">
					<fo:block text-align="center" font-weight="bold" font-size="8pt" color="white"> DŁUGOŚĆ ODC. </fo:block>
                </fo:table-cell>
				<fo:table-cell padding="5px" border="0.5pt solid black" display-align="center">
					<fo:block text-align="center" font-weight="bold" font-size="8pt" color="white"> OCENA SPOŁECZNOŚCI </fo:block>
                </fo:table-cell>
				<fo:table-cell padding="5px" border="0.5pt solid black" display-align="center">
					<fo:block text-align="center" font-weight="bold" font-size="8pt" color="white"> NASZA OCENA </fo:block>
                </fo:table-cell>
			</fo:table-row>
			<xsl:apply-templates select="anime"/>
		</fo:table-body>
	</fo:table>
</xsl:template>

<xsl:template match="anime">
	<fo:table-row keep-together.within-page="always" background-color="rgb(190,190,190)">
		<fo:table-cell border="0.5pt solid black" display-align="center">
			<fo:block text-align="center" font-size="8pt" padding-top="3px" padding-bottom="3px" padding-left="6px" padding-right="6px">
				<xsl:value-of select="nazwa"/>
			</fo:block>
		</fo:table-cell>
		<fo:table-cell border="0.5pt solid black" display-align="center">
			<fo:block text-align="center" font-size="8pt" padding-top="3px" padding-bottom="3px" padding-left="6px" padding-right="6px">
				<xsl:value-of select="studio"/>
			</fo:block>
		</fo:table-cell>
		<fo:table-cell border="0.5pt solid black" display-align="center">
			<fo:block text-align="center" font-size="8pt" padding-top="3px" padding-bottom="3px" padding-left="6px" padding-right="6px">
				<xsl:value-of select="data_wydania"/>
			</fo:block>
		</fo:table-cell>
		<fo:table-cell border="0.5pt solid black" width="50px" display-align="center">
			<fo:block text-align="center" font-size="8pt" padding-top="3px" padding-bottom="3px" padding-left="6px" padding-right="6px">
				<xsl:value-of select="typ"/>
			</fo:block>
		</fo:table-cell>
		<fo:table-cell border="0.5pt solid black" display-align="center">
			<fo:block text-align="center" font-size="8pt" padding-top="3px" padding-bottom="3px" padding-left="6px" padding-right="6px">
				<xsl:value-of select="gatunki"/>
			</fo:block>
		</fo:table-cell>
		<fo:table-cell border="0.5pt solid black" display-align="center">
			<fo:block text-align="center" font-size="8pt" padding-top="3px" padding-bottom="3px" padding-left="6px" padding-right="6px">
				<xsl:value-of select="liczba_odcinków"/>
			</fo:block>
		</fo:table-cell>
		<fo:table-cell border="0.5pt solid black" display-align="center">
			<fo:block text-align="center" font-size="8pt" padding-top="3px" padding-bottom="3px" padding-left="6px" padding-right="6px">
				<xsl:value-of select="długość_odcinka"/>
			</fo:block>
		</fo:table-cell>
		<fo:table-cell border="0.5pt solid black" display-align="center">
			<fo:block text-align="center" font-size="8pt" padding-top="3px" padding-bottom="3px" padding-left="6px" padding-right="6px">
				<xsl:value-of select="ocena_społeczności"/>
			</fo:block>
		</fo:table-cell>
		<fo:table-cell border="0.5pt solid black" display-align="center">
			<fo:block text-align="center" font-size="8pt" padding-top="3px" padding-bottom="3px" padding-left="6px" padding-right="6px">
				<xsl:value-of select="nasza_ocena"/>
			</fo:block>
		</fo:table-cell>
	</fo:table-row>
</xsl:template>

<xsl:template match="statystyki">
	<fo:list-block provisional-distance-between-starts="0.25cm" provisional-label-separation="0.15cm" font-family="Courier New" font-size="14pt" font-style="italic" text-align="left">
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba źródeł: <xsl:value-of select="liczba_źródeł"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime: <xsl:value-of select="liczba_anime"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>  
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba studiów: <xsl:value-of select="liczba_studiów"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>  
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Suma odcinków: <xsl:value-of select="suma_odcinków"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
                    <fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
                </fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Łączna liczba minut: <xsl:value-of select="łączna_liczba_minut"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Średnia ocen społeczności: <xsl:value-of select="średnia_ocen_społeczności"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Średnia naszych ocen: <xsl:value-of select="średnia_naszych_ocen"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba gatunków: <xsl:value-of select="liczba_gatunków"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>  
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "akcja": <xsl:value-of select="liczba_anime_akcja"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>  
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "dramat": <xsl:value-of select="liczba_anime_dramat"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>  
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "fantasy": <xsl:value-of select="liczba_anime_fantasy"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>  
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "historia": <xsl:value-of select="liczba_anime_historia"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>  
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "horror": <xsl:value-of select="liczba_anime_horror"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>  
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "komedia": <xsl:value-of select="liczba_anime_komedia"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>  
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "muzyka": <xsl:value-of select="liczba_anime_muzyka"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>  
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "okruchy życia": <xsl:value-of select="liczba_anime_okruchy_życia"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "przygoda": <xsl:value-of select="liczba_anime_przygoda"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "psychologia": <xsl:value-of select="liczba_anime_psychologia"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "romans": <xsl:value-of select="liczba_anime_romans"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "science fiction": <xsl:value-of select="liczba_anime_science_fiction"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "sport": <xsl:value-of select="liczba_anime_sport"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "tajemnica": <xsl:value-of select="liczba_anime_tajemnica"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
		<fo:list-item>
			<fo:list-item-label end-indent="label-end()">
				<fo:block>
					<fo:inline font-family="Courier" font-size="10pt">•</fo:inline>
				</fo:block>
			</fo:list-item-label>
			<fo:list-item-body start-indent="body-start()">
				<fo:block>
					Liczba anime z gatunku "zjawiska nadprzyrodzone": <xsl:value-of select="liczba_anime_zjawiska_nadprzyrodzone"/>
				</fo:block>
			</fo:list-item-body>
		</fo:list-item> 
	</fo:list-block>
</xsl:template>

<xsl:template match="autorzy">
	<fo:block font-family="Times New Roman" font-size="17pt" font-weight="bold" text-align="center" color="rgb(220,20,60)">
		<xsl:apply-templates select="autor[@specjalizacja='TGSK']"/>
	</fo:block>
	<fo:block font-family="Times New Roman" font-size="17pt" font-weight="bold" text-align="center" color="rgb(65,105,225)">
		<xsl:apply-templates select="autor[@specjalizacja='IOAD']"/>
	</fo:block>
</xsl:template>

<xsl:template match="autor">
		<xsl:value-of select="concat(imię, ' ', nazwisko, ' - ', numer_indeksu)"/>
</xsl:template>

</xsl:stylesheet>