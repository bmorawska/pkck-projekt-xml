<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
				xmlns:exsl="http://exslt.org/common"
                xmlns:anime="http://www.animeNamespace.com"
				extension-element-prefixes="exsl"
                exclude-result-prefixes="anime">

	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
				
	<xsl:key name="studioTworząceAnime" match="studio" use="@studioID"/>
	<xsl:key name="typAnime" match="typ" use="@typID"/>

	<xsl:template match="/anime:dokument">
            <dokument>
                <xsl:apply-templates select="link"/>
                <xsl:apply-templates select="tytuł"/>
                <xsl:apply-templates select="autorzy"/>
                <xsl:apply-templates select="anime_dane"/>
				<statystyki>
					<liczba_źródeł>
						<xsl:value-of select="count(anime_dane/źródła/źródło)"/> 
					</liczba_źródeł>
					<liczba_anime>
						<xsl:value-of select="count(anime_dane/spis_anime/animation)"/>
					</liczba_anime>
					<liczba_studiów>
						<xsl:value-of select="count(anime_dane/studia/studio)"/> 
					</liczba_studiów>
					<liczba_gatunków>
						<xsl:value-of select="count(anime_dane/gatunki/gatunek)"/> 
					</liczba_gatunków>
					<liczba_anime_akcja>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Action'])"/>
					</liczba_anime_akcja>
					<liczba_anime_dramat>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Drama'])"/>
					</liczba_anime_dramat>
					<liczba_anime_fantasy>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Fantasy'])"/>
					</liczba_anime_fantasy>
					<liczba_anime_historia>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Historical'])"/>
					</liczba_anime_historia>
					<liczba_anime_horror>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Horror'])"/>
					</liczba_anime_horror>
					<liczba_anime_komedia>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Comedy'])"/>
					</liczba_anime_komedia>
					<liczba_anime_muzyka>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Music'])"/>
					</liczba_anime_muzyka>
					<liczba_anime_okruchy_życia>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='SliceOfLife'])"/>
					</liczba_anime_okruchy_życia>
					<liczba_anime_przygoda>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Adventure'])"/>
					</liczba_anime_przygoda>
					<liczba_anime_psychologia>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Psychological'])"/>
					</liczba_anime_psychologia>
					<liczba_anime_romans>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Romance'])"/>
					</liczba_anime_romans>
					<liczba_anime_science_fiction>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Scifi'])"/>
					</liczba_anime_science_fiction>
					<liczba_anime_sport>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Sports'])"/>
					</liczba_anime_sport>
					<liczba_anime_tajemnica>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Mystery'])"/>
					</liczba_anime_tajemnica>
					<liczba_anime_zjawiska_nadprzyrodzone>
						<xsl:value-of select="count(anime_dane/spis_anime/animation[informacje/lista_gatunków/gatunek_ref/@gatunekIDREF='Supernatural'])"/>
					</liczba_anime_zjawiska_nadprzyrodzone>
					<suma_odcinków>
						<xsl:value-of select="sum(anime_dane/spis_anime/animation/informacje/liczba_odcinków)"/>
					</suma_odcinków>
					
					<xsl:variable name="liczbaMinutAnime">
						<xsl:for-each select="anime_dane/spis_anime/animation">
							<number>
								<xsl:value-of select="informacje/liczba_odcinków * informacje/długość"/>
							</number>
						</xsl:for-each>
					</xsl:variable>
					
					<łączna_liczba_minut>
						<xsl:value-of select="sum(exsl:node-set($liczbaMinutAnime)/number)"/>
					</łączna_liczba_minut>
					<średnia_ocen_społeczności>
						<xsl:value-of select="format-number(sum(anime_dane/spis_anime/animation/ocena/średnia_ocena) div count(anime_dane/spis_anime/animation), '0.00')"/>
					</średnia_ocen_społeczności>
					<średnia_naszych_ocen>
						<xsl:value-of select="format-number(sum(anime_dane/spis_anime/animation/ocena/moja_ocena) div count(anime_dane/spis_anime/animation), '0.00')"/>
					</średnia_naszych_ocen>
					
				</statystyki>
            </dokument>
    </xsl:template>
	
	<!-- ANIME DANE -->
	<xsl:template match="anime_dane">
		<xsl:apply-templates select="źródła"/>
		<xsl:apply-templates select="spis_anime"/>
		<xsl:apply-templates select="typy"/>
		<xsl:apply-templates select="gatunki"/>
		<xsl:apply-templates select="studia"/>
		
	</xsl:template>
	
	<!-- NIEUŻYWANE -->
	<xsl:template match="spis_anime"/>
	<xsl:template match="typy"/>
	<xsl:template match="gatunki"/>
	<xsl:template match="studia"/>
	
	<!-- TYTUŁ -->
	<xsl:template match="tytuł">
		<tytuł>
			<xsl:value-of select="."/>
		</tytuł>
	</xsl:template>
	
	
	<!-- LINK -->
	<xsl:template match="link">
		<link>
			<xsl:attribute name="href">
				<xsl:value-of select="@href"/>
			</xsl:attribute>
		</link>
	</xsl:template>
	
	<!-- ŹRÓDŁA -->    
    <xsl:template match="źródła">
        <spis_anime>
            <xsl:for-each select="źródło">
                <źródło>
                    <xsl:variable name="źródło_id" select="@źródłoID" />
                    <xsl:attribute name="źródłoID">
                        <xsl:value-of select="$źródło_id"/>
                    </xsl:attribute>
                    <nazwa_źródła>
                        <xsl:value-of select="."/>
                    </nazwa_źródła>
                    <pozycje_anime>
                        <xsl:apply-templates select="../../spis_anime/animation[informacje/źródło_ref/@źródłoIDREF=$źródło_id]">
                            <xsl:sort select="informacje/nazwa" data-type="text" order="ascending"/>
                        </xsl:apply-templates>
                    </pozycje_anime>
                </źródło>
            </xsl:for-each>
        </spis_anime>
    </xsl:template>
	
	<!-- ANIME -->
	<xsl:template match="animation">
		<anime>
			<nazwa>
				<xsl:value-of select="informacje/nazwa"/>
            </nazwa>
			<nazwa_oryginalna>
				<xsl:value-of select="informacje/nazwa_oryginalna"/>
            </nazwa_oryginalna>
			<studio>
				<xsl:for-each select="key('studioTworząceAnime', @studioIDREF)">
                    <xsl:value-of select="nazwa_studia"/>
                </xsl:for-each>
            </studio>
			<data_wydania>
				<xsl:value-of select="concat(informacje/data_wydania/@dzień, '-', informacje/data_wydania/@miesiąc, '-', informacje/data_wydania/@rok)"/>
			</data_wydania>
			<typ> 
				<xsl:for-each select="key('typAnime', informacje/typ_ref/@typIDREF)">
                    <xsl:value-of select="."/>
                </xsl:for-each>
			</typ>
			<gatunki>
				<xsl:for-each select="informacje/lista_gatunków/gatunek_ref">
					<xsl:choose>
						<xsl:when test="position() != last()">
							<xsl:value-of select="concat(., ', ')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="."/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</gatunki>
			<liczba_odcinków>
				<xsl:value-of select="informacje/liczba_odcinków"/>
			</liczba_odcinków>
			<długość_odcinka>
				<xsl:value-of select="concat(informacje/długość, ' ', informacje/długość/@jednostka, '.')"/>
			</długość_odcinka>
			<ocena_społeczności>
				<xsl:value-of select="ocena/średnia_ocena"/>
			</ocena_społeczności>
			<nasza_ocena>
				<xsl:value-of select="ocena/moja_ocena"/>
			</nasza_ocena>
		</anime>
	</xsl:template>
	
	<xsl:template match="autorzy">
		<autorzy>
			<xsl:for-each select="autor">
				<autor>
					<xsl:attribute name="semestr">
						<xsl:value-of select="@semestr" />
					</xsl:attribute>
					<xsl:attribute name="specjalizacja">
						<xsl:value-of select="@specjalizacja" />
					</xsl:attribute>
					<imię> 
						<xsl:value-of select="imię"/>
					</imię>
					<nazwisko> 
						<xsl:value-of select="nazwisko"/>
					</nazwisko>
					<numer_indeksu>
						<xsl:value-of select="indeks"/>
					</numer_indeksu>
				</autor>
			</xsl:for-each>
		</autorzy>
	</xsl:template>

</xsl:stylesheet>