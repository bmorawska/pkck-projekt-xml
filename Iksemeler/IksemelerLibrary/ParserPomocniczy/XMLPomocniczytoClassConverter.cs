﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IksemelerLibrary.ParserPomocniczy
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class dokument
    {

        private string linkField;

        private string tytułField;

        private ObservableCollection<dokumentAutor> autorzyField;

        private ObservableCollection<dokumentŹródło> spis_animeField;

        private dokumentStatystyki statystykiField;

        /// <remarks/>
        public string link
        {
            get
            {
                return this.linkField;
            }
            set
            {
                this.linkField = value;
            }
        }

        /// <remarks/>
        public string tytuł
        {
            get
            {
                return this.tytułField;
            }
            set
            {
                this.tytułField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("autor", IsNullable = false)]
        public ObservableCollection<dokumentAutor> autorzy
        {
            get
            {
                return this.autorzyField;
            }
            set
            {
                this.autorzyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("źródło", IsNullable = false)]
        public ObservableCollection<dokumentŹródło> spis_anime
        {
            get
            {
                return this.spis_animeField;
            }
            set
            {
                this.spis_animeField = value;
            }
        }

        /// <remarks/>
        public dokumentStatystyki statystyki
        {
            get
            {
                return this.statystykiField;
            }
            set
            {
                this.statystykiField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class dokumentLink
    {

        private string hrefField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string href
        {
            get
            {
                return this.hrefField;
            }
            set
            {
                this.hrefField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class dokumentAutor
    {

        private string imięField;

        private string nazwiskoField;

        private uint numer_indeksuField;

        private byte semestrField;

        private string specjalizacjaField;

        /// <remarks/>
        public string imię
        {
            get
            {
                return this.imięField;
            }
            set
            {
                this.imięField = value;
            }
        }

        /// <remarks/>
        public string nazwisko
        {
            get
            {
                return this.nazwiskoField;
            }
            set
            {
                this.nazwiskoField = value;
            }
        }

        /// <remarks/>
        public uint numer_indeksu
        {
            get
            {
                return this.numer_indeksuField;
            }
            set
            {
                this.numer_indeksuField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte semestr
        {
            get
            {
                return this.semestrField;
            }
            set
            {
                this.semestrField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string specjalizacja
        {
            get
            {
                return this.specjalizacjaField;
            }
            set
            {
                this.specjalizacjaField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class dokumentŹródło
    {

        private string nazwa_źródłaField;

        private ObservableCollection<dokumentŹródłoAnime> pozycje_animeField;

        private string źródłoIDField;

        /// <remarks/>
        public string nazwa_źródła
        {
            get
            {
                return this.nazwa_źródłaField;
            }
            set
            {
                this.nazwa_źródłaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("anime", IsNullable = false)]
        public ObservableCollection<dokumentŹródłoAnime> pozycje_anime
        {
            get
            {
                return this.pozycje_animeField;
            }
            set
            {
                this.pozycje_animeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string źródłoID
        {
            get
            {
                return this.źródłoIDField;
            }
            set
            {
                this.źródłoIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class dokumentŹródłoAnime
    {

        private string nazwaField;

        private string nazwa_oryginalnaField;

        private string studioField;

        private string data_wydaniaField;

        private string typField;

        private string gatunkiField;

        private byte liczba_odcinkówField;

        private string długość_odcinkaField;

        private decimal ocena_społecznościField;

        private decimal nasza_ocenaField;

        /// <remarks/>
        public string nazwa
        {
            get
            {
                return this.nazwaField;
            }
            set
            {
                this.nazwaField = value;
            }
        }

        /// <remarks/>
        public string nazwa_oryginalna
        {
            get
            {
                return this.nazwa_oryginalnaField;
            }
            set
            {
                this.nazwa_oryginalnaField = value;
            }
        }

        /// <remarks/>
        public string studio
        {
            get
            {
                return this.studioField;
            }
            set
            {
                this.studioField = value;
            }
        }

        /// <remarks/>
        public string data_wydania
        {
            get
            {
                return this.data_wydaniaField;
            }
            set
            {
                this.data_wydaniaField = value;
            }
        }

        /// <remarks/>
        public string typ
        {
            get
            {
                return this.typField;
            }
            set
            {
                this.typField = value;
            }
        }

        /// <remarks/>
        public string gatunki
        {
            get
            {
                return this.gatunkiField;
            }
            set
            {
                this.gatunkiField = value;
            }
        }

        /// <remarks/>
        public byte liczba_odcinków
        {
            get
            {
                return this.liczba_odcinkówField;
            }
            set
            {
                this.liczba_odcinkówField = value;
            }
        }

        /// <remarks/>
        public string długość_odcinka
        {
            get
            {
                return this.długość_odcinkaField;
            }
            set
            {
                this.długość_odcinkaField = value;
            }
        }

        /// <remarks/>
        public decimal ocena_społeczności
        {
            get
            {
                return this.ocena_społecznościField;
            }
            set
            {
                this.ocena_społecznościField = value;
            }
        }

        /// <remarks/>
        public decimal nasza_ocena
        {
            get
            {
                return this.nasza_ocenaField;
            }
            set
            {
                this.nasza_ocenaField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class dokumentStatystyki
    {

        private byte liczba_źródełField;

        private byte liczba_animeField;

        private byte liczba_studiówField;

        private byte liczba_gatunkówField;

        private byte liczba_anime_akcjaField;

        private byte liczba_anime_dramatField;

        private byte liczba_anime_fantasyField;

        private byte liczba_anime_historiaField;

        private byte liczba_anime_horrorField;

        private byte liczba_anime_komediaField;

        private byte liczba_anime_muzykaField;

        private byte liczba_anime_okruchy_życiaField;

        private byte liczba_anime_przygodaField;

        private byte liczba_anime_psychologiaField;

        private byte liczba_anime_romansField;

        private byte liczba_anime_science_fictionField;

        private byte liczba_anime_sportField;

        private byte liczba_anime_tajemnicaField;

        private byte liczba_anime_zjawiska_nadprzyrodzoneField;

        private ushort suma_odcinkówField;

        private ushort łączna_liczba_minutField;

        private decimal średnia_ocen_społecznościField;

        private decimal średnia_naszych_ocenField;

        /// <remarks/>
        public byte liczba_źródeł
        {
            get
            {
                return this.liczba_źródełField;
            }
            set
            {
                this.liczba_źródełField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime
        {
            get
            {
                return this.liczba_animeField;
            }
            set
            {
                this.liczba_animeField = value;
            }
        }

        /// <remarks/>
        public byte liczba_studiów
        {
            get
            {
                return this.liczba_studiówField;
            }
            set
            {
                this.liczba_studiówField = value;
            }
        }

        /// <remarks/>
        public byte liczba_gatunków
        {
            get
            {
                return this.liczba_gatunkówField;
            }
            set
            {
                this.liczba_gatunkówField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_akcja
        {
            get
            {
                return this.liczba_anime_akcjaField;
            }
            set
            {
                this.liczba_anime_akcjaField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_dramat
        {
            get
            {
                return this.liczba_anime_dramatField;
            }
            set
            {
                this.liczba_anime_dramatField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_fantasy
        {
            get
            {
                return this.liczba_anime_fantasyField;
            }
            set
            {
                this.liczba_anime_fantasyField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_historia
        {
            get
            {
                return this.liczba_anime_historiaField;
            }
            set
            {
                this.liczba_anime_historiaField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_horror
        {
            get
            {
                return this.liczba_anime_horrorField;
            }
            set
            {
                this.liczba_anime_horrorField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_komedia
        {
            get
            {
                return this.liczba_anime_komediaField;
            }
            set
            {
                this.liczba_anime_komediaField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_muzyka
        {
            get
            {
                return this.liczba_anime_muzykaField;
            }
            set
            {
                this.liczba_anime_muzykaField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_okruchy_życia
        {
            get
            {
                return this.liczba_anime_okruchy_życiaField;
            }
            set
            {
                this.liczba_anime_okruchy_życiaField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_przygoda
        {
            get
            {
                return this.liczba_anime_przygodaField;
            }
            set
            {
                this.liczba_anime_przygodaField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_psychologia
        {
            get
            {
                return this.liczba_anime_psychologiaField;
            }
            set
            {
                this.liczba_anime_psychologiaField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_romans
        {
            get
            {
                return this.liczba_anime_romansField;
            }
            set
            {
                this.liczba_anime_romansField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_science_fiction
        {
            get
            {
                return this.liczba_anime_science_fictionField;
            }
            set
            {
                this.liczba_anime_science_fictionField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_sport
        {
            get
            {
                return this.liczba_anime_sportField;
            }
            set
            {
                this.liczba_anime_sportField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_tajemnica
        {
            get
            {
                return this.liczba_anime_tajemnicaField;
            }
            set
            {
                this.liczba_anime_tajemnicaField = value;
            }
        }

        /// <remarks/>
        public byte liczba_anime_zjawiska_nadprzyrodzone
        {
            get
            {
                return this.liczba_anime_zjawiska_nadprzyrodzoneField;
            }
            set
            {
                this.liczba_anime_zjawiska_nadprzyrodzoneField = value;
            }
        }

        /// <remarks/>
        public ushort suma_odcinków
        {
            get
            {
                return this.suma_odcinkówField;
            }
            set
            {
                this.suma_odcinkówField = value;
            }
        }

        /// <remarks/>
        public ushort łączna_liczba_minut
        {
            get
            {
                return this.łączna_liczba_minutField;
            }
            set
            {
                this.łączna_liczba_minutField = value;
            }
        }

        /// <remarks/>
        public decimal średnia_ocen_społeczności
        {
            get
            {
                return this.średnia_ocen_społecznościField;
            }
            set
            {
                this.średnia_ocen_społecznościField = value;
            }
        }

        /// <remarks/>
        public decimal średnia_naszych_ocen
        {
            get
            {
                return this.średnia_naszych_ocenField;
            }
            set
            {
                this.średnia_naszych_ocenField = value;
            }
        }
    }
}
