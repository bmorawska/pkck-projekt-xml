﻿using IksemelerLibrary.Parser;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using IksemelerLibrary.Transformations;
using IksemelerLibrary.ValueConverters;

namespace GUI.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        #region Properties

        private readonly Dispatcher _dispatcher;
        private IksemelerLibrary.Parser.dokument dokument;
        private IksemelerLibrary.ParserPomocniczy.dokument dokumentPomocniczy;

        public IksemelerLibrary.Parser.dokument Dokument
        {
            get => dokument;
            set
            {
                dokument = value;
                OnPropertyChanged();
            }
        }

        // Nazwa pliku XML
        private String _fileName;

        public String filename
        {
            get => _fileName;
            set
            {
                _fileName = value;
                OnPropertyChanged();
            }
        }

        // Nazwa pliku xsd
        private String _schemafileName;

        public String schemafilename
        {
            get => _schemafileName;
            set
            {
                _schemafileName = value;
                OnPropertyChanged();
            }
        }

        // Zmienna do odblokowania guzika zapisu jeśli podano xsd i xsl.
        private bool _zapiszButtonActive;
        public bool zapiszButtonActive
        {
            get => _zapiszButtonActive;
            set
            {
                _zapiszButtonActive = value;
                OnPropertyChanged();
            }
        }

        private bool isSchema = false;  //Zmienna pomocnicza do _zapiszButtonActive
        private bool isXMLFile = false; //Zmienna pomocnicza do _zapiszButtonActive

        //Lista możliwych przekształceń
        public List<String> AvailableTransformations { get; set; }
        // Pierwsze, domyślnie zaznaczone przekształcenie
        public String SelectedTypeFirst { get; set; }

        //Ścieżka pliku do zapisu przekształcenia
        private String _outputTransformationsFile { get; set; }
        public String outputTransformationsFile
        {
            get => _outputTransformationsFile;
            set
            {
                _outputTransformationsFile = value;
                OnPropertyChanged();
            }
        }


        //Ścieżka do pliku z plikiem xsl
        private String _xslFileName;
        public String xslFileName
        {
            get => _xslFileName;
            set
            {
                _xslFileName = value;
                OnPropertyChanged();
            }
        }

        //Ścieżka do pliku pomocniczego
        private String _helpfilename;
        public String helpfilename
        {
            get => _helpfilename;
            set
            {
                _helpfilename = value;
                OnPropertyChanged();
            }
        }

        //Zmienna sterująca aktywacją guzika do transformacji
        private bool _transformujButtonActive;
        public bool transformujButtonActive
        {
            get => _transformujButtonActive;
            set
            {
                _transformujButtonActive = value;
                OnPropertyChanged();
            }
        }

        private bool isHelpXml = false;  //Zmienna pomocnicza do _transformujButtonActive
        private bool isXsl = false; //Zmienna pomocnicza do _transformujButtonActive
        private bool isOutput = false;  //Zmienna pomocnicza do _transformujButtonActive


        private ObservableCollection<anime_daneAnimation> _animeDaneAnimations;
        public ObservableCollection<anime_daneAnimation> animeDaneAnimations
        {
            get => _animeDaneAnimations;
            set
            {
                _animeDaneAnimations = value;
                OnPropertyChanged();
            }
        }

        private anime_daneAnimation _selectedAnime;
        public anime_daneAnimation selectedAnime
        {
            get => _selectedAnime;
            set
            {
                _selectedAnime = value;
                OnPropertyChanged();
            }
        }

        //Pola do dodawania i edycji 
        private String _nazwaEditableField;
        public String nazwaEditableField
        {
            get => _nazwaEditableField;
            set
            {
                _nazwaEditableField = value;
                OnPropertyChanged();
            }
        }

        private String _nazwaOryginalnaEditableField;
        public String nazwaOryginalnaEditableField
        {
            get => _nazwaOryginalnaEditableField;
            set
            {
                _nazwaOryginalnaEditableField = value;
                OnPropertyChanged();
            }
        }

        private String _typEditableField;
        public String typEditableField
        {
            get => _typEditableField;
            set
            {
                _typEditableField = value;
                OnPropertyChanged();
            }
        }

        private List<String> _typyListaEditableField;
        public List<String> typyListaEditableField
        {
            get => _typyListaEditableField;
            set
            {
                _typyListaEditableField = value;
                OnPropertyChanged();
            }
        }


        private String _źródłoEditableField;
        public String źródłoEditableField
        {
            get => _źródłoEditableField;
            set
            {
                _źródłoEditableField = value;
                OnPropertyChanged();
            }
        }

        private List<String> _źródłoListaEditableField;
        public List<String> źródłoListaEditableField
        {
            get => _źródłoListaEditableField;
            set
            {
                _źródłoListaEditableField = value;
                OnPropertyChanged();
            }
        }

        private DateTime _dataEditableField;
        public DateTime dataEditableField
        {
            get => _dataEditableField;
            set
            {
                _dataEditableField = value;
                OnPropertyChanged();
            }
        }

        private String _listaGatunkówGatunekEditableField;
        public String listaGatunkówGatunekEditableField
        {
            get => _listaGatunkówGatunekEditableField;
            set
            {
                _listaGatunkówGatunekEditableField = value;
                OnPropertyChanged();
            }
        }


        private List<String> _listaGatunkówEditableField;
        public List<String> listaGatunkówEditableField
        {
            get => _listaGatunkówEditableField;
            set
            {
                _listaGatunkówEditableField = value;
                OnPropertyChanged();
            }
        }

        private List<String> _listaDodanychGatunkówEditableField;
        public List<String> listaDodanychGatunkówEditableField
        {
            get => _listaDodanychGatunkówEditableField;
            set
            {
                _listaDodanychGatunkówEditableField = value;
                OnPropertyChanged();
            }
        }

        private String _selectedAnimeType;
        public String selectedAnimeType
        {
            get => _selectedAnimeType;
            set
            {
                _selectedAnimeType = value;
                OnPropertyChanged();
            }
        }


        private int _liczbaOdcinkówEditableField;
        public int liczbaOdcinkówEditableField
        {
            get => _liczbaOdcinkówEditableField;
            set
            {
                _liczbaOdcinkówEditableField = value;
                OnPropertyChanged();
            }
        }

        private int _długośćEditableField;
        public int długośćEditableField
        {
            get => _długośćEditableField;
            set
            {
                _długośćEditableField = value;
                OnPropertyChanged();
            }
        }

        private double _naszaOcenaEditableField;
        public double naszaOcenaEditableField
        {
            get => _naszaOcenaEditableField;
            set
            {
                _naszaOcenaEditableField = value;
                OnPropertyChanged();
            }
        }

        private double _ocenaEditableField;
        public double ocenaEditableField
        {
            get => _ocenaEditableField;
            set
            {
                _ocenaEditableField = value;
                OnPropertyChanged();
            }
        }

        private List<String> _listaStudiówEditableField;
        public List<String> listaStudiówEditableField
        {
            get => _listaStudiówEditableField;
            set
            {
                _listaStudiówEditableField = value;
                OnPropertyChanged();
            }
        }

        private String _studioEditableField;
        public String studioEditableField
        {
            get => _studioEditableField;
            set
            {
                _studioEditableField = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region RelayCommands

        public RelayCommand LoadXmlFromFileCommand { get; }
        public RelayCommand SaveXmlToFileCommand { get; }
        public RelayCommand LoadSchemaFromFileCommand { get; }
        public RelayCommand SaveOutputTransformationFileCommand { get; }
        public RelayCommand LoadXslFromFileCommand { get; }
        public RelayCommand TransformCommand { get; }
        public RelayCommand LoadHelpFileNameFromFileCommand { get; }
        public RelayCommand AddNewAnimeCommand { get; }
        public RelayCommand EditAnimeCommand { get; }
        public RelayCommand DeleteAnimeCommand { get; }
        public RelayCommand AddAnimeTypeToListCommand { get; }
        public RelayCommand ClearAnimeCommand { get; }
        public RelayCommand CopyFieldsToEditionCommand { get; }
        public RelayCommand DeleteAnimeTypeFromListCommand { get; }

        #endregion

        #region Constructors

        public MainViewModel()
        {
            this._dispatcher = Dispatcher.CurrentDispatcher;
            this.LoadXmlFromFileCommand = new RelayCommand(param => LoadXmlFromFile(param), null);
            this.SaveXmlToFileCommand = new RelayCommand(param => SaveXmlToFile(param), null);
            this.LoadSchemaFromFileCommand = new RelayCommand(param => LoadSchemaFromFile(param), null);
            this.AvailableTransformations = new List<string>
            {
                "XHTML",
                "TXT",
                "SVG",
                "PDF", 
                "Inny XML"
            };
            //this.SelectedTypeFirst = AvailableTransformations[0];
            this.SelectedTypeFirst = AvailableTransformations[0];
            this.SaveOutputTransformationFileCommand = new RelayCommand(param => SaveOutputTransformationFile(param), null);
            this.LoadXslFromFileCommand = new RelayCommand(param => LoadXslFromFile(param), null);
            this.TransformCommand = new RelayCommand(param => Transform(param), null);
            this.LoadHelpFileNameFromFileCommand = new RelayCommand(param => LoadHelpFileNameFromFile(param), null);
            this.AddNewAnimeCommand = new RelayCommand(param => AddNewAnime(param), null);
            this.EditAnimeCommand = new RelayCommand(param => EditAnime(param), null);
            this.DeleteAnimeCommand = new RelayCommand(param => DeleteAnime(param), null);
            this.AddAnimeTypeToListCommand = new RelayCommand(param => AddAnimeTypeToList(param), null);
            this.źródłoListaEditableField = new List<string>
            {
               "Produkcja oryginalna",
               "Manga",
               "Light Novel",
               "Visual Novel",
               "Gra",
               "Powieść"
            };

            this.źródłoEditableField = źródłoListaEditableField[0];
            this.typyListaEditableField = new List<string>
            {
                "Seria TV",
                "Film",
                "OVA"
            };
            this.typEditableField = typyListaEditableField[0];
            this.listaGatunkówEditableField = new List<string>
            {
                "Akcja",
                "Dramat",
                "Fantasy",
                "Historia",
                "Horror",
                "Komedia",
                "Muzyka",
                "Okruchy życia",
                "Przygoda",
                "Psychologia",
                "Romans",
                "Science Fiction",
                "Sport",
                "Tajemnica",
                "Zjawiska nadprzyrodzone"
            };
            this.listaGatunkówGatunekEditableField = listaGatunkówEditableField[0];
            this.listaStudiówEditableField = new List<string>
            {
                "Nexus",
                "A1",
                "Passione",
                "WF",
                "IG",
                "CoMix",
                "DK",
                "JCS",
                "Madhouse",
                "Lerche",
                "CW",
                "KyoAni",
                "Tezuka",
                "Deen",
                "Xebec",
                "Bandai",
                "C4",
                "Satelight",
                "Shaft",
                "Nomad",
                "Asread",
                "PA",
                "Pierrot",
                "Ghibli"
            };
            this.studioEditableField = listaStudiówEditableField[0];
            this.ClearAnimeCommand = new RelayCommand(param => ClearAnime(param), null);
            this.CopyFieldsToEditionCommand = new RelayCommand(param => CopyFieldsToEdition(param), null);
            this.DeleteAnimeTypeFromListCommand = new RelayCommand(param => DeleteAnimeTypeFromList(param), null);
        }

        #endregion

        #region Methods

        private void LoadXmlFromFile(object param)
        {
            // Otwieramy okienko do wyboru pliku
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog
            {
                Multiselect = false,
                RestoreDirectory = true,
                Title = "Wybierz plik XML",
                DefaultExt = "xml",
                Filter = "XML Files | *.xml"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                filename = openFileDialog.FileName;
                isXMLFile = true;
                changeZapiszButtonOnTrueIfPossible();
            }

            // Wczytujemy xml jako obiekty
            dokument = new IksemelerLibrary.Parser.dokument();
            XmlSerializer serializer = new XmlSerializer(typeof(IksemelerLibrary.Parser.dokument));

            using (FileStream fileStream = new FileStream(filename, FileMode.Open))
            {
                dokument = (IksemelerLibrary.Parser.dokument)serializer.Deserialize(fileStream);
            }

            animeDaneAnimations = dokument.anime_dane.spis_anime;
        }

        private void SaveXmlToFile(object param)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(dokument));

            //Kopiujemy tymczasowy dokument xml do pliku tmp.xml, żeby zobaczyć czy jest poprawny.
            //Plik tmp będzie się znajdował w tym samym katalogu co wczytany plik xml.
            string temporaryFilePath = filename;
            int index = temporaryFilePath.LastIndexOf("/");
            if (index > 0)
            {
                temporaryFilePath = temporaryFilePath.Substring(0, index);
            }
            temporaryFilePath = temporaryFilePath + "tmp.xml";

            using (XmlTextWriter writer = new XmlTextWriter(temporaryFilePath, System.Text.Encoding.UTF8))
            {
                serializer.Serialize(writer, dokument);
            }

            if (ValidateSchema(temporaryFilePath))
            {
                // Jeśli walidacja przebiegła pomyślnie to nadpisujemy stary plik plikiem tymczasowym.
                File.Copy(temporaryFilePath, filename, true);
                // I usuwamy plik tymczasowy.
                File.Delete(temporaryFilePath);
                System.Windows.Forms.MessageBox.Show("Dokument poprawny. Zapisano zmiany.",
                                                     "Walidacja schema poprawna.",
                                                      MessageBoxButtons.OK,
                                                      MessageBoxIcon.Information);
            }
            else
            {
                // Jeśli nie to usuwamy plik tymczasowy, bo i tak jest niepoprawny.
                File.Delete(temporaryFilePath);
            }
        }

        private bool ValidateSchema(string xmlPath)
        {
            // From https://www.youtube.com/watch?v=wTgSS8X90aA
            XmlReader xmlReader = null;
            try
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                //TODO Zrobić żeby sam znajdował sobie target.
                settings.Schemas.Add("http://www.animeNamespace.com", schemafilename);
                settings.ValidationType = ValidationType.Schema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                settings.ValidationEventHandler +=
                    new System.Xml.Schema.ValidationEventHandler(this.ValidationEventHandler);


                // Walidacja z powyższymi ustawieniami
                xmlReader = XmlReader.Create(xmlPath, settings);
                while (xmlReader.Read())
                {
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
            }
        }

        private void ValidationEventHandler(object sender, ValidationEventArgs args)
        {
            // Jak jesteś w tej funkcji to coś poszło nie tak i xml nie działa :-( 
            Console.WriteLine("\r\n\tValidation Error: " + args.Message);
            System.Windows.Forms.MessageBox.Show(args.Message + "\nDokument niepoprawny. Nie zapisano zmian.",
                                                "Walidacja schema zakończona niepowodzeniem.",
                                                 MessageBoxButtons.OK,
                                                 MessageBoxIcon.Exclamation);
            throw new Exception("Validation Error: " + args.Message);
        }
             
        private void LoadSchemaFromFile(object param)
        {
            // Otwieramy okienko do wyboru pliku
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog
            {
                Multiselect = false,
                RestoreDirectory = true,
                Title = "Wybierz plik Schema",
                DefaultExt = "xsd",
                Filter = "XSD Files | *.xsd"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                schemafilename = openFileDialog.FileName;
                isSchema = true;
                changeZapiszButtonOnTrueIfPossible();
            }
        }

        private void changeZapiszButtonOnTrueIfPossible()
        {
            zapiszButtonActive = (isXMLFile && isSchema);
        }

        private void Transform(object param)
        {
            ITransformation transformation;

            switch (SelectedTypeFirst)
            {
                case "XHTML":
                {
                    transformation = new XHTMLTransformation();
                    transformation.Transform(outputTransformationsFile, xslFileName, helpfilename);
                    break;
                }
                case "SVG":
                {
                    transformation = new SVGTransformation();
                    transformation.Transform(outputTransformationsFile, xslFileName, helpfilename);
                    break;
                }
                case "TXT":
                {
                    transformation = new TXTTransformation();
                    transformation.Transform(outputTransformationsFile, xslFileName, helpfilename);
                    break;
                }
                case "Inny XML":
                {
                    transformation = new XMLTransformation();
                    transformation.Transform(outputTransformationsFile, xslFileName, filename);
                    break;
                }
                case "PDF":
                {
                    transformation = new PDFTransformation();
                    transformation.Transform(outputTransformationsFile, xslFileName, helpfilename);
                    break;
                }
                default:
                {
                    System.Windows.Forms.MessageBox.Show("Coś poszło nie tak :-/. Może restart programu pomoże...",
                    "Coś poszło nie tak",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                    return;
                }
            }

            System.Windows.Forms.MessageBox.Show("Transformacja udana",
                "Transformacja udana",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void SaveOutputTransformationFile(object param)
        {
            System.Windows.Forms.SaveFileDialog saveFileDialog = new System.Windows.Forms.SaveFileDialog
            {
                RestoreDirectory = true,
                Title = "Wybierz miejsce zapisu pliku po transformacji",
                Filter = "All Files (*.*)|*.*"
            };
          
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
               outputTransformationsFile = saveFileDialog.FileName;
               outputTransformationsFile = AddExtension(outputTransformationsFile);
               isOutput = true;
               changeTransformujButtonOnTrueIfPossible();
            }
        }

        private string AddExtension(string outputTransformationsFile)
        {
            //Sprawdzamy, czy ktoś samemu nie wpisał rozszerzenia. Jak tak to usuwamy.
            int index = outputTransformationsFile.LastIndexOf(".");
            if (index > 0)
            {
                outputTransformationsFile = outputTransformationsFile.Substring(0, index);
            }

            //Dodajemy rozszerzenie
            string extension = null;
            if (SelectedTypeFirst.Equals("Inny XML"))
            {
                extension = ".xml";
            }
            else
            {
                extension = "." + SelectedTypeFirst.ToLower();
            }

            outputTransformationsFile += extension;
            return (outputTransformationsFile);
        }

        
        private void LoadXslFromFile(object param)
        {
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog
            {
                Multiselect = false,
                RestoreDirectory = true,
                Title = "Wybierz plik Schema",
                DefaultExt = "xsl",
                Filter = "XSL Files | *.xsl"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                xslFileName = openFileDialog.FileName;
                isXsl = true;
                changeTransformujButtonOnTrueIfPossible();
            }
        }

        private void LoadHelpFileNameFromFile(object param)
        {
            // Otwieramy okienko do wyboru pliku
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog
            {
                Multiselect = false,
                RestoreDirectory = true,
                Title = "Wybierz plik XML pomocniczy",
                DefaultExt = "xml",
                Filter = "XML Files | *.xml"
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                helpfilename = openFileDialog.FileName;
                isHelpXml = true;
                changeTransformujButtonOnTrueIfPossible();
            }

            // Wczytujemy xml jako obiekty
            dokumentPomocniczy = new IksemelerLibrary.ParserPomocniczy.dokument();
            XmlSerializer serializer = new XmlSerializer(typeof(IksemelerLibrary.ParserPomocniczy.dokument));

            using (FileStream fileStream = new FileStream(helpfilename, FileMode.Open))
            {
                dokumentPomocniczy = (IksemelerLibrary.ParserPomocniczy.dokument)serializer.Deserialize(fileStream);
            }
        }
        private void changeTransformujButtonOnTrueIfPossible()
        {
            transformujButtonActive= (isOutput && isXsl && isHelpXml);
        }

        private void AddNewAnime(object param)
        {
            ValueConverter vc = new ValueConverter();

            anime_daneAnimation anime = new anime_daneAnimation();
            anime.studioIDREF = studioEditableField;

            //INFORMACJE
                anime_daneAnimationInformacje informacje = new anime_daneAnimationInformacje();
                informacje.nazwa = nazwaEditableField;
                informacje.nazwa_oryginalna = nazwaOryginalnaEditableField;

                    anime_daneAnimationInformacjeTyp_ref typ = new anime_daneAnimationInformacjeTyp_ref();
                    typ.Value = typEditableField;
                    typ.typIDREF = vc.GetTypIDREFfromValue(typEditableField);

                    anime_daneAnimationInformacjeŹródło_ref źródło = new anime_daneAnimationInformacjeŹródło_ref();
                    źródło.Value = źródłoEditableField;
                    źródło.źródłoIDREF = vc.GetŹródłoIDREFfromValue(źródłoEditableField);

                    anime_daneAnimationInformacjeData_wydania data = new anime_daneAnimationInformacjeData_wydania();
                    data.dzień = vc.GetDzieńFromDate(dataEditableField);
                    data.miesiąc = vc.GetMiesiącFromDate(dataEditableField);
                    data.rok = vc.GetRokFromDate(dataEditableField);

                    ObservableCollection<anime_daneAnimationInformacjeGatunek_ref> gatunki = vc.GetCollectionOfGatunki(listaDodanychGatunkówEditableField);

                    anime_daneAnimationInformacjeDługość długość = new anime_daneAnimationInformacjeDługość();
                    długość.jednostka = vc.GetJednostka();
                    długość.Value = (byte)długośćEditableField;

                informacje.typ_ref = typ;
                informacje.źródło_ref = źródło;
                informacje.data_wydania = data;
                informacje.lista_gatunków = gatunki;
                informacje.długość = długość;
                informacje.liczba_odcinków = (byte)liczbaOdcinkówEditableField;

            //OCENA    
                anime_daneAnimationOcena ocena = new anime_daneAnimationOcena();
                ocena.moja_ocena = (decimal)naszaOcenaEditableField;
                ocena.średnia_ocena = (decimal)ocenaEditableField;

            anime.informacje = informacje;
            anime.ocena = ocena;

            animeDaneAnimations.Add(anime);

            System.Windows.Forms.MessageBox.Show("Dodano anime",
                "Dodano anime",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void EditAnime(object param)
        {
            ValueConverter vc = new ValueConverter();

            selectedAnime.studioIDREF = studioEditableField;

            //INFORMACJE
            anime_daneAnimationInformacje informacje = new anime_daneAnimationInformacje();
            informacje.nazwa = nazwaEditableField;
            informacje.nazwa_oryginalna = nazwaOryginalnaEditableField;

            anime_daneAnimationInformacjeTyp_ref typ = new anime_daneAnimationInformacjeTyp_ref();
            typ.Value = typEditableField;
            typ.typIDREF = vc.GetTypIDREFfromValue(typEditableField);

            anime_daneAnimationInformacjeŹródło_ref źródło = new anime_daneAnimationInformacjeŹródło_ref();
            źródło.Value = źródłoEditableField;
            źródło.źródłoIDREF = vc.GetŹródłoIDREFfromValue(źródłoEditableField);

            anime_daneAnimationInformacjeData_wydania data = new anime_daneAnimationInformacjeData_wydania();
            data.dzień = vc.GetDzieńFromDate(dataEditableField);
            data.miesiąc = vc.GetMiesiącFromDate(dataEditableField);
            data.rok = vc.GetRokFromDate(dataEditableField);

            ObservableCollection<anime_daneAnimationInformacjeGatunek_ref> gatunki = vc.GetCollectionOfGatunki(listaDodanychGatunkówEditableField);

            anime_daneAnimationInformacjeDługość długość = new anime_daneAnimationInformacjeDługość();
            długość.jednostka = vc.GetJednostka();
            długość.Value = (byte)długośćEditableField;

            informacje.typ_ref = typ;
            informacje.źródło_ref = źródło;
            informacje.data_wydania = data;
            informacje.lista_gatunków = gatunki;
            informacje.długość = długość;
            informacje.liczba_odcinków = (byte)liczbaOdcinkówEditableField;

            //OCENA    
            anime_daneAnimationOcena ocena = new anime_daneAnimationOcena();
            ocena.moja_ocena = (decimal)naszaOcenaEditableField;
            ocena.średnia_ocena = (decimal)ocenaEditableField;

            selectedAnime.informacje = informacje;
            selectedAnime.ocena = ocena;

            System.Windows.Forms.MessageBox.Show("Zmieniono dane",
                "Zmieniono dane anime",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void DeleteAnime(object param)
        {
            animeDaneAnimations.Remove(selectedAnime);
        }

        private void AddAnimeTypeToList(object param)
        {
           if (this.listaDodanychGatunkówEditableField == null)
           {
              this.listaDodanychGatunkówEditableField = new List<string>();
           }

           List<String> tempList = new List<String>();
           foreach (var gatunek in listaDodanychGatunkówEditableField)
           {
               tempList.Add(gatunek);
           }

            String nowyGatunek = listaGatunkówGatunekEditableField;
            if (!tempList.Contains(nowyGatunek))
            {
                tempList.Add(nowyGatunek);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Ten gatunek jest już na liście",
                    "Błąd dodawania gatunku",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }

            listaDodanychGatunkówEditableField = tempList;

        }

        private void ClearAnime(object param)
        {
            studioEditableField = listaStudiówEditableField[0];
            nazwaEditableField = "";
            nazwaOryginalnaEditableField = "";
            typEditableField = typyListaEditableField[0];
            źródłoEditableField = źródłoListaEditableField[0];
            dataEditableField = DateTime.Today;
            listaDodanychGatunkówEditableField = null;
            liczbaOdcinkówEditableField = 0;
            długośćEditableField = 0;
            naszaOcenaEditableField = 0;
            ocenaEditableField = 0;
        }

        private void CopyFieldsToEdition(object param)
        {
            studioEditableField = selectedAnime.studioIDREF;
            nazwaEditableField = selectedAnime.informacje.nazwa;
            nazwaOryginalnaEditableField = selectedAnime.informacje.nazwa_oryginalna;
            typEditableField = selectedAnime.informacje.typ_ref.Value;
            źródłoEditableField = selectedAnime.informacje.źródło_ref.Value;
            dataEditableField = new DateTime(Int32.Parse(selectedAnime.informacje.data_wydania.rok),
                                             Int32.Parse(selectedAnime.informacje.data_wydania.miesiąc),
                                             Int32.Parse(selectedAnime.informacje.data_wydania.dzień));


            listaDodanychGatunkówEditableField = new List<string>();
            foreach (var gatunek in selectedAnime.informacje.lista_gatunków)
            {
                listaDodanychGatunkówEditableField.Add(gatunek.Value);
            }

            anime_daneAnimationOcena ocena = new anime_daneAnimationOcena();
            ocena.moja_ocena = selectedAnime.ocena.moja_ocena;
            ocena.średnia_ocena = selectedAnime.ocena.średnia_ocena;

            liczbaOdcinkówEditableField = selectedAnime.informacje.liczba_odcinków;
            długośćEditableField = selectedAnime.informacje.długość.Value;
            naszaOcenaEditableField = (double)ocena.moja_ocena;
            ocenaEditableField = (double)ocena.średnia_ocena;
        }

        private void DeleteAnimeTypeFromList(object param)
        { 
            List<String> tempList = new List<String>();
            foreach (var gatunek in listaDodanychGatunkówEditableField)
            {
                tempList.Add(gatunek);
            }

            String gatunekDoUsuniecia = selectedAnimeType;
            if (tempList.Contains(gatunekDoUsuniecia))
            {
                tempList.Remove(gatunekDoUsuniecia);
            }

            listaDodanychGatunkówEditableField = tempList;
        }

        #endregion
    }
}
