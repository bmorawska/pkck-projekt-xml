<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns="http://www.w3.org/1999/xhtml">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"
                doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
                doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

	<xsl:template match="/dokument">
		<html>
			<head>
				<title> Spis anime </title>
			</head>
			<body style="background-color: SnowWhite">
				<div>
                    <h1 style="text-align: center">
						<xsl:value-of select="tytuł"/>
					</h1>
                </div>
				<div style="text-align:center">
					<h2 style="text-align: center">SPIS TREŚCI</h2>
					<ol style="list-style-type: disc; display: inline-block; text-align:left">
                       <li>Spis treści</li>
                       <li><a href="#p_original">Produkcje oryginalne</a></li>
                       <li><a href="#p_manga">Produkcje na podstawie mangi</a></li>
                       <li><a href="#p_lightnovel">Produkcje na podstawie light novel</a></li>
                       <li><a href="#p_visualnovel">Produkcje na podstawie visual novel</a></li>
                       <li><a href="#p_game">Produkcje na podstawie gry</a></li>
                       <li><a href="#p_novel">Produkcje na podstawie powieści</a></li>
					   <li><a href="#stats">Podsumowanie (statystyki)</a></li>
					   <li><a href="#autorzy">Autorzy</a></li>
                   </ol>
				</div>
               <div>
                  <h2 style="text-align: center"> <a id="p_original">PRODUKCJE ORYGINALNE</a></h2>
                   <xsl:apply-templates select="spis_anime/źródło[@źródłoID='Original']"/>
               </div>
               <div>
                   <h2 style="text-align: center"><a id="p_manga">PRODUKCJE NA PODSTAWIE MANGI</a></h2>
                   <xsl:apply-templates select="spis_anime/źródło[@źródłoID='Manga']"/>
               </div>
               <div>
                   <h2 style="text-align: center"><a id="p_lightnovel">PRODUKCJE NA PODSTAWIE LIGHT NOVEL</a></h2>
                   <xsl:apply-templates select="spis_anime/źródło[@źródłoID='LN']"/>
               </div>
               <div>
                   <h2 style="text-align: center"><a id="p_visualnovel">PRODUKCJE NA PODSTAWIE VISUAL NOVEL</a></h2>
                   <xsl:apply-templates select="spis_anime/źródło[@źródłoID='VN']"/>
               </div>
               <div>
                   <h2 style="text-align: center"><a id="p_game">PRODUKCJE NA PODSTAWIE GRY</a></h2>
                   <xsl:apply-templates select="spis_anime/źródło[@źródłoID='Game']"/>
               </div>
			   <div>
                   <h2 style="text-align: center"><a id="p_novel">PRODUKCJE NA PODSTAWIE POWIEŚCI</a></h2>
                   <xsl:apply-templates select="spis_anime/źródło[@źródłoID='Novel']"/>
               </div>
               <div>
                   <h2 style="text-align: center"><a id="stats">PODSUMOWANIE (STATYSTYKI)</a></h2>
                   <xsl:apply-templates select="statystyki"/>
               </div>
               <div>
				   <h2 style="text-align: center"><a id="autorzy">AUTORZY</a></h2>
                   <xsl:apply-templates select="autorzy"/>
               </div>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="autorzy">
		<table style="width: 600px; margin-left: auto; margin-right: auto">
			<tr>
				<td style="width: 300px; color: Crimson">
					<xsl:apply-templates select="autor[@specjalizacja='TGSK']"/>
				</td>
				<td style="width: 300px; color: RoyalBlue">
					<xsl:apply-templates select="autor[@specjalizacja='IOAD']"/>
				</td>
			</tr>
		</table>
	</xsl:template>
	
	<xsl:template match="autor">
		<h3 style="text-align: center">
            <xsl:value-of select="concat(imię, ' ', nazwisko, ' - ', numer_indeksu)"/>
        </h3>
	</xsl:template>
	
	<xsl:template match="źródło">
		<span style="display:block; height: 20px"/>
		<xsl:apply-templates select="pozycje_anime/anime"/>
	</xsl:template>
	
	<xsl:template match="anime">
		<div>
			<table border="1" style="width: 450px; margin-left: auto; margin-right: auto">
				<tr style="background-color: Silver">
					<td style="width: 150px">
						<b>Nazwa:</b>
					</td>
					<td style="padding-left: 5px; padding-right: 5px">
						<b><xsl:value-of select="nazwa"/></b>
					</td>
				</tr>
				<tr style="background-color: LightGrey">
					<td style="width: 150px">
						<b>Nazwa oryginalna:</b>
					</td>
					<td style="font-family: 'Sawarabi Gothic', sans-serif; padding-left: 5px; padding-right: 5px">
						<xsl:value-of select="nazwa_oryginalna"/>
					</td>
				</tr>
				<tr style="background-color: Silver">
					<td style="width: 150px">
						<b>Studio:</b>
					</td>
					<td style="padding-left: 5px; padding-right: 5px">
						<xsl:value-of select="studio"/>
					</td>
				</tr>
				<tr style="background-color: LightGrey">
					<td style="width: 150px">
						<b>Data wydania:</b>
					</td>
					<td style="padding-left: 5px; padding-right: 5px">
						<xsl:value-of select="data_wydania"/>
					</td>
				</tr>
				<tr style="background-color: Silver">
					<td style="width: 150px">
						<b>Typ:</b>
					</td>
					<td style="padding-left: 5px; padding-right: 5px">
						<xsl:value-of select="typ"/>
					</td>
				</tr>
				<tr style="background-color: LightGrey">
					<td style="width: 150px">
						<b>Liczba odcinków:</b>
					</td>
					<td style="padding-left: 5px; padding-right: 5px">
						<xsl:value-of select="liczba_odcinków"/>
					</td>
				</tr>
				<tr style="background-color: Silver">
					<td style="width: 150px">
						<b>Długość odcinka:</b>
					</td>
					<td style="padding-left: 5px; padding-right: 5px">
						<xsl:value-of select="długość_odcinka"/>
					</td>
				</tr>
				<tr style="background-color: LightGrey">
					<td style="width: 150px">
						<b>Ocena społeczności:</b>
					</td>
					<td style="padding-left: 5px; padding-right: 5px">
						<xsl:value-of select="ocena_społeczności"/>
					</td>
				</tr>
				<tr style="background-color: Silver">
					<td style="width: 150px">
						<b>Nasza ocena:</b>
					</td>
					<td style="padding-left: 5px; padding-right: 5px">
						<xsl:value-of select="nasza_ocena"/>
					</td>
				</tr>
				<tr style="background-color: LightGrey">
					<td colspan="2" style="text-align: center; padding-left: 5px; padding-right: 5px;">
						<xsl:value-of select="gatunki"/>
					</td>
				</tr>
			</table>
		</div>
        <span style="display:block; height: 25px"></span>
	</xsl:template>
	
	<xsl:template match="statystyki">
		<div style="text-align: center">
			<div style="display: inline-block; text-align:left">
				<p> Lista jest podzielona na <b><xsl:value-of select="liczba_źródeł"/> kategorii</b> W skład listy wchodzi <b><xsl:value-of select="liczba_anime"/> produkcji</b>. Wymienione anime zostały wykonane przez <b><xsl:value-of select="liczba_studiów"/> różnych studiów </b>. </p>
				<p> Produkcje mają w sumie <b><xsl:value-of select="suma_odcinków"/> odcinków</b>, których czas trwa łącznie <b><xsl:value-of select="łączna_liczba_minut"/> minut</b>. Animacje w naszej liście zawierają <b><xsl:value-of select="liczba_gatunków"/> różnych gatunków</b>.</p>
				<p> Średnia ocen społeczności wynosi <b><xsl:value-of select="średnia_ocen_społeczności"/></b>, podczas gdy średnia naszych ocen to <b><xsl:value-of select="średnia_naszych_ocen"/></b>.</p>
				<p> Przynależność anime do poszczególnych gatunków (jedna produkcja może należeć do kilku gatunków jednocześnie): </p>
				<div style="display: block; text-align: center">
					<ol style="list-style-type: decimal; display: inline-block; text-align:left">
						<li> Akcja - <b><xsl:value-of select="liczba_anime_akcja"/> anime</b>.</li>
						<li> Dramat - <b><xsl:value-of select="liczba_anime_dramat"/> anime</b>.</li>
						<li> Fantasy - <b><xsl:value-of select="liczba_anime_fantasy"/> anime</b>.</li>
						<li> Historia - <b><xsl:value-of select="liczba_anime_historia"/> anime</b>.</li>
						<li> Horror - <b><xsl:value-of select="liczba_anime_horror"/> anime</b>.</li>
						<li> Komedia - <b><xsl:value-of select="liczba_anime_komedia"/> anime</b>.</li>
						<li> Muzyka - <b><xsl:value-of select="liczba_anime_muzyka"/> anime</b>.</li>
						<li> Okruchy życia - <b><xsl:value-of select="liczba_anime_okruchy_życia"/> anime</b>.</li>
						<li> Przygoda - <b><xsl:value-of select="liczba_anime_przygoda"/> anime</b>.</li>
						<li> Psychologia - <b><xsl:value-of select="liczba_anime_psychologia"/> anime</b>.</li>
						<li> Romans - <b><xsl:value-of select="liczba_anime_romans"/> anime</b>.</li>
						<li> Science-fiction - <b><xsl:value-of select="liczba_anime_science_fiction"/> anime</b>.</li>
						<li> Sport - <b><xsl:value-of select="liczba_anime_sport"/> anime</b>.</li>
						<li> Tajemnica - <b><xsl:value-of select="liczba_anime_tajemnica"/> anime</b>.</li>
						<li> Zjawiska nadprzyrodzone - <b><xsl:value-of select="liczba_anime_zjawiska_nadprzyrodzone"/> anime</b>.</li>
					</ol>
				</div>
			</div>
		</div>
		<span style="display:block; height: 20px"></span>
    </xsl:template>  
</xsl:stylesheet>